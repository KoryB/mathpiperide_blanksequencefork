package org.mathpiper.lisp.astprocessors;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.lisp.cons.NumberCons;
import org.mathpiper.lisp.tokenizers.MathPiperTokenizer;


public class ObjectToMetaSubstitute
        implements ASTProcessor {

    Environment iEnvironment;
    private Map options;

    public ObjectToMetaSubstitute(Environment aEnvironment, Map options) {
        iEnvironment = aEnvironment;
        this.options = options;
    }


    public Cons patternMatches(Environment aEnvironment, int aStackTop, Cons aElement, List<Integer> positionList)
            throws Throwable {
	
	if(aElement instanceof NumberCons)
	{
	    return null;
	}
	
	
	if(aElement.car() instanceof String)
	{
	    String atomName = ((String) aElement.car());
	    
            boolean isSymbolic = false;
            
            for(char c : atomName.toCharArray())
            {
                if(MathPiperTokenizer.isSymbolic(c))
                {
                    isSymbolic = true;
                    break;
                }
                    
            }
            
	    if(! isSymbolic && ! atomName.contains("_") && aEnvironment.isVariable(atomName))
	    {
		atomName = "_".concat(atomName);
            }
            else if(! atomName.contains("$") && aEnvironment.isProcedure(atomName) && (Boolean) options.get("Procedures?"))
            {
                atomName = atomName.concat("$");
            }
	    
            Cons newCons = AtomCons.getInstance(aEnvironment.getPrecision(), atomName);
            
            if(aElement.getMetadataMap() != null)
            {
                newCons.setMetadataMap(new HashMap<String,Object>(aElement.getMetadataMap()));
            }
            
	    return(newCons);
	}

        return null;
    }

}
