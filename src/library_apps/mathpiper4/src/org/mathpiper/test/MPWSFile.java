/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
package org.mathpiper.test;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MPWSFile {

    public MPWSFile() {
        super();
    }

    
    public static Map<String, Fold> getFoldsMap(String sourceName, InputStream inputStream) throws Throwable {
        return getFoldsMap(sourceName, inputStream, null, null);
    }//end method.

    
    public static Map<String, Fold> getFoldsMap(String sourceName, InputStream inputStream, String type) throws Throwable {
        return getFoldsMap(sourceName, inputStream, type, null);
    }//end method.        

    
    public static Map<String, Fold> getFoldsMap(String sourceName, InputStream inputStream, String foldType, String foldSubtype) throws Throwable {
        Map<String, Fold> namedFolds = new HashMap<String, Fold>();

        List<Fold> folds = scanSourceFile(sourceName, inputStream);

        for (Fold fold : folds) {
            Map<String, String> attributes = fold.getAttributes();

            Set<String> keys = attributes.keySet();

            for (String key : keys) {

                if (key.equalsIgnoreCase("name")) {
                    
                    String foldName = attributes.get(key);
                    
                    if(foldType == null && foldSubtype == null)
                    {
                        namedFolds.put(foldName, fold);
                    }
                    else if(foldType != null && foldSubtype == null)
                    {
                        String currentFoldType = fold.getType();
                        
                        if(currentFoldType != null && currentFoldType.equals(foldType))
                        {
                            namedFolds.put(foldName, fold);
                        }
                    }
                    else if(foldType != null && foldSubtype != null)
                    {
                        String currentFoldType = fold.getType();
                        
                        String currentFoldSubtype = attributes.get("subtype");
                        
                        if(currentFoldType != null && currentFoldSubtype != null && currentFoldType.equals(foldType) && currentFoldSubtype.equals(foldSubtype))
                        {
                            namedFolds.put(foldName, fold);
                        }
                    }
                }
            }
        }

        return namedFolds;

    }//end method.
    
    
    public static List<Fold> scanSourceFile(String sourceName, InputStream inputStream) throws Throwable {
        return scanSourceFile(sourceName, inputStream, false);
    } 

    
    public static List<Fold> scanSourceFile(String sourceName, InputStream inputStream, boolean isReverse) throws Throwable {

        //Uncomment for debugging.
        /*
         if (getFileName.equals("Factors.mpw")) {
         int xxx = 1;
         }//end if.*/
        
        List<Fold> folds = new ArrayList<Fold>();
        StringBuilder foldContents = new StringBuilder();
        String foldHeader = "";
        boolean inFold = false;
        boolean inOutputFold = false;
        int lineCounter = 0;
        int startLineNumber = -1;


        DataInputStream in = new DataInputStream(inputStream);
        
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            
            String line;
            
            //Read File Line By Line
            while ((line = br.readLine()) != null) {

                //System.out.println(line);
                lineCounter++;

                if (!line.matches("^ */%/.*$") && !line.matches("^ *\\. */%/.*$") && line.contains("%/")) {

                    String tempLine = line;

                    if (tempLine.startsWith(".")) {
                        tempLine = line.substring(1);

                        if (tempLine.trim().startsWith("%/output")) {
                            inOutputFold = false;
                        }
                    }

                    if (!inOutputFold && tempLine.trim().startsWith("%/") && !line.trim().startsWith("%/group")) {

                        if (inFold == false) {
                            throw new Exception("Opening fold tag missing in file " + sourceName + " on line " + lineCounter);
                        }

                        Fold fold = new Fold(startLineNumber, lineCounter, foldHeader, foldContents.toString());
                        foldContents.delete(0, foldContents.length());
                        folds.add(fold);
                        inFold = false;
                    }

                } else if (!inOutputFold && line.trim().startsWith("%") && line.trim().length() > 2 && line.trim().charAt(1) != ' ' && !line.trim().startsWith("%group")) {

                    if (inFold == true) {
                        throw new Exception("Closing fold tag missing in file " + sourceName + " on line " + lineCounter);
                    }

                    if (line.trim().startsWith("%output")) {
                        inOutputFold = true;
                    }

                    startLineNumber = lineCounter;
                    foldHeader = line.trim();
                    inFold = true;
                } else if (inFold == true) {
                    foldContents.append(line);
                    foldContents.append("\n");

                }
            }

            if (inFold == true) {
                throw new Exception("Opening or closing fold tag missing in file " + sourceName + " on line " + lineCounter);
            }

            if(isReverse) Collections.reverse(folds);

            return folds;
        }
        finally
        {
            in.close();
        }
    }//end.

    public static void main(String[] args) {
        File mpwFile = new File("/home/tkosan/git/mathpiper/experimental/automatic_grading/etco1120/worksheets/lectures/week13/points_patterns_exercises_3.mpwnl");
        
        try {
            // Map map = org.mathpiper.test.MPWSFile.getFoldsMap("Problem", new FileInputStream(mpwFile), "mathpiper", "problem");
   
            List<Fold> folds = scanSourceFile(mpwFile.getPath(), new FileInputStream(mpwFile));

            for (Fold fold : folds) {
                Map attributes = fold.getAttributes();

                Set keys = attributes.keySet();

                for (Object key : keys) {
                    System.out.println(key.toString() + " - " + attributes.get(key));

                    if (attributes.get(key).equals("information")) {
                        String code = fold.getContents();

                        System.out.println(code);

                    }
                }
            }

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

}
