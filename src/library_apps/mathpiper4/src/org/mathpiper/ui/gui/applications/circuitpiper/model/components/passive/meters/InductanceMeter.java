package org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters;

import org.mathpiper.ui.gui.applications.circuitpiper.view.DrawingPanel;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ScaledGraphics;
/*AUTHORS:

 - Kevin Stueve (2009-12-20): initial published version
 #*****************************************************************************
 #       Copyright (C) 2009 Kevin Stueve kstueve@uw.edu
 #
 #  Distributed under the terms of the GNU General Public License (GPL)
 #                  http://www.gnu.org/licenses/
 #*****************************************************************************
 */

public final class InductanceMeter extends Meter {

    public static String inductanceStringInitial = "??? H";
    public String inductanceString = inductanceStringInitial;

    public InductanceMeter(int x, int y, String uid, DrawingPanel drawingPanel) {
        super(x, y, drawingPanel);
        if(uid == null)
        {
            componentUID = componentCounter++ + "";
        }
        else
        {
            componentUID = uid;
            
            try{
                int number = Integer.parseInt(uid);
        
                if(number >= componentCounter)
                {
                    componentCounter = number + 1;
                }
            }
            catch (NumberFormatException nfe)
            {
            }
        }
        primary = "inductance";
    }
    
    public void reset() {
        super.reset();
        inductanceString = InductanceMeter.inductanceStringInitial;
    }

    public void draw(ScaledGraphics sg) {
        super.draw(sg);
        int x1 = headTerminal.getX();
        int x2 = tailTerminal.getX();
        int y1 = headTerminal.getY();
        int y2 = tailTerminal.getY();
        int rise = y2 - y1;
        int run = x2 - x1;
        double distance = Math.sqrt(rise * rise + run * run);
        double xm = (x1 + x2) / 2;
        double ym = (y1 + y2) / 2;
        if (distance >= 2 * METER_SIZE) {
            sg.drawLine(x1, y1,  (x1 + run / 2.0 - METER_SIZE * run / distance),  (y1 + rise / 2 - METER_SIZE * rise / distance));
            sg.drawLine( (x2 - run / 2.0 + METER_SIZE * run / distance),  (y2 - rise / 2.0 + METER_SIZE * rise / distance), x2, y2);
            sg.drawOval( (xm - METER_SIZE),  (ym - METER_SIZE), 2 * METER_SIZE, 2 * METER_SIZE);
        } else {
            sg.drawOval( (xm - distance / 2),  (ym - distance / 2),  distance,  distance);
        }
    }
}
