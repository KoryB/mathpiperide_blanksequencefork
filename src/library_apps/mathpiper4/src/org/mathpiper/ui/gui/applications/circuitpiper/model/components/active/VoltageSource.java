package org.mathpiper.ui.gui.applications.circuitpiper.model.components.active;

import java.util.Stack;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.Component;
import static org.mathpiper.ui.gui.applications.circuitpiper.model.components.Component.siToValue;
import org.mathpiper.ui.gui.applications.circuitpiper.view.DrawingPanel;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ScaledGraphics;

/*AUTHORS:

 - Kevin Stueve (2009-12-20): initial published version
 #*****************************************************************************
 #       Copyright (C) 2009 Kevin Stueve kstueve@uw.edu
 #
 #  Distributed under the terms of the GNU General Public License (GPL)
 #                  http://www.gnu.org/licenses/
 #*****************************************************************************
 */
public final class VoltageSource extends Component {
    
    public static int componentCounter = 1;

    double voltage;

    public VoltageSource(int x, int y, String uid, DrawingPanel drawingPanel) {
        super(x, y, drawingPanel);
        if(uid == null)
        {
            componentUID = componentCounter++ + "";
        }
        else
        {
            componentUID = uid;
            
            try{
                int number = Integer.parseInt(uid);
        
                if(number >= componentCounter)
                {
                    componentCounter = number + 1;
                }
            }
            catch (NumberFormatException nfe)
            {
            }
        }
        init();
        primaryValue = 4.56;
        enteredPrimaryValue = "" + siToValue.get(siPrefix) * primaryValue;
    }
    
    public VoltageSource(int x, int y, String uid, Stack<String> attributes, DrawingPanel drawingPanel) throws Exception {
        super(x, y, drawingPanel);
        if(uid == null)
        {
            componentUID = componentCounter++ + "";
        }
        else
        {
            componentUID = uid;
            
            try{
                int number = Integer.parseInt(uid);
        
                if(number >= componentCounter)
                {
                    componentCounter = number + 1;
                }
            }
            catch (NumberFormatException nfe)
            {
            }
        }
        init();
        
        handleAttributes(attributes);
    }

    public void init() {
        primary = "Voltage";
        primaryUnit = "Volt";
        primaryUnitPlural = "Volts";
        primarySymbol = "V";
        siPrefix = "";
        primaryUnitSymbol = "V";
        preselectedPrimaryPrefix = siPrefix  + primaryUnitSymbol;
    }

    public void draw(ScaledGraphics sg) {
        super.draw(sg);
        
        double size = 18;
        
        int x1 = headTerminal.getX();
        int x2 = tailTerminal.getX();
        int y1 = headTerminal.getY();
        int y2 = tailTerminal.getY();
        int rise = y2 - y1;
        int run = x2 - x1;
        double distance = Math.sqrt(rise * rise + run * run);
        double xm = (x1 + x2) / 2;
        double ym = (y1 + y2) / 2;
        if (distance >= 2 * size) {
            sg.drawLine(x1, y1,  (x1 + run / 2.0 - size * run / distance),  (y1 + rise / 2 - size * rise / distance));
            sg.drawLine( (x2 - run / 2.0 + size * run / distance),  (y2 - rise / 2.0 + size * rise / distance), x2, y2);
            sg.drawOval( (xm - size),  (ym - size), 2 * size, 2 * size);
            
            sg.drawString("+",  (x1 + run / 2.0 - (size + 10) * run / distance - 10 * rise / distance),
                     (y1 + rise / 2 - (size + 10) * rise / distance + 10 * run / distance));
        } else {
            sg.drawOval( (xm - distance / 2),  (ym - distance / 2),  distance,  distance);
        }
    }
    
    public String toString()
    {
        return super.toString() +  " " + this.primaryValue;
    }
    
}
