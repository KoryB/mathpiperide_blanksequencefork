package org.mathpiper.ui.gui.applications.voscilloscope.oscilloscope;

import org.mathpiper.ui.gui.applications.voscilloscope.simulator.BooleanBitmap;
import org.mathpiper.ui.gui.applications.voscilloscope.simulator.RotateBitmap;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;
import java.util.EventObject;

/**
 * <p>Title: Virtual Oscilloscope.</p>
 * <p>Description: A Oscilloscope simulator</p>
 * <p>Copyright (C) 2003 José Manuel Gómez Soriano</p>
 * <h2>License</h2>
 * <p>
 This file is part of Virtual Oscilloscope.

 Virtual Oscilloscope is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Virtual Oscilloscope is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Virtual Oscilloscope; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 * </p>
 */

// Referenced classes of package es.upv.scope:
//            PTime, PVolt, PQuality, Screen

public class PBackground extends Panel
    implements MouseListener
{

    public static Color relleno;
    public static Color background;
    public static Color panel;
    public PTime pTime;
    public PVolt pVolt;
    public PQuality pQuality;
    public Screen screen;

    public PBackground()
    {
        relleno = new Color(90, 90, 90);
        background = new Color(120, 120, 120);
        panel = new Color(80, 80, 100);
        setBackground(Color.white);
        setForeground(panel);
        setLayout(null);
        pTime = new PTime();
        add(pTime);
        pTime.setLocation(373, 12);
        pVolt = new PVolt();
        add(pVolt);
        pVolt.setLocation(373, 189);
        pQuality = new PQuality();
        add(pQuality);
        pQuality.setLocation(15, 335);
        pQuality.BtnDigital.addMouseListener(this);
        screen = new Screen();
        add(screen);
        screen.setBounds(38, 20, 300, 300); //todo:tk:the size of the scope display can be adjusted here.
        addMouseListener(this);
    }

    public void addNotify()
    {
        super.addNotify();
    }

    public Dimension getMinimumSize()
    {
        return new Dimension(780, 400);
    }

    public Dimension getPreferredSize()
    {
        return getMinimumSize();
    }

    public void setLocation(int x, int y)
    {
        super.setLocation(x, y);
        setSize(getMinimumSize());
    }

    public void paint(Graphics g)
    {
        drawBackground(g);
    }

    public void mouseClicked(MouseEvent e)
    {
        if(e.getSource() == pQuality.BtnDigital)
        {
            pVolt.BtnCiclica1.setVisible(!pQuality.BtnDigital.getValue());
            pVolt.BtnCiclica2.setVisible(!pQuality.BtnDigital.getValue());
            pTime.btnXPos.setValue(19F);
            pTime.btnXPos.setVisible(!pQuality.BtnDigital.getValue());
            pTime.btnHoldOff.setVisible(!pQuality.BtnDigital.getValue());
            pTime.btnMasMenos.setVisible(!pQuality.BtnDigital.getValue());
            pTime.btnLevel.setVisible(!pQuality.BtnDigital.getValue());
            pTime.btnAtNormal.setVisible(!pQuality.BtnDigital.getValue());
            pTime.btnPosicion.setVisible(pQuality.BtnDigital.getValue());
            pTime.lPosicion.setVisible(pQuality.BtnDigital.getValue());
        }
    }

    public void mouseEntered(MouseEvent mouseevent)
    {
    }

    public void mouseExited(MouseEvent mouseevent)
    {
    }

    public void mousePressed(MouseEvent mouseevent)
    {
    }

    public void mouseReleased(MouseEvent mouseevent)
    {
    }

    public void drawBackground(Graphics g)
    {
        g.setColor(relleno);
        g.fillRoundRect(0, 0, 780, 400, 45, 45);
        g.setColor(background);
        g.fillRoundRect(4, 4, 772, 392, 45, 45);
        g.setColor(relleno);
        g.fillRoundRect(25, 7, 327, 327, 20, 20);
        int x1[] = {
            14, 25, 25
        };
        int y1[] = {
            172, 17, 324
        };
        g.fillPolygon(x1, y1, 3);
        int x2[] = {
            363, 352, 352
        };
        int y2[] = {
            172, 17, 324
        };
        g.fillPolygon(x2, y2, 3);
        g.setColor(background);
        g.fillRoundRect(29, 11, 319, 319, 20, 20);
        g.setColor(Color.black);
        g.fillRoundRect(33, 15, 310, 310, 15, 15);
    }

    public void update(Graphics g)
    {
        paint(g);
    }
}
