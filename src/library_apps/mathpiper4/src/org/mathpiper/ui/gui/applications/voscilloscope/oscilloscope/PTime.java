package org.mathpiper.ui.gui.applications.voscilloscope.oscilloscope;

import org.mathpiper.ui.gui.applications.voscilloscope.simulator.RotateBitmap;
import org.mathpiper.ui.gui.applications.voscilloscope.simulator.BooleanBitmap;
import org.mathpiper.ui.gui.applications.voscilloscope.simulator.Message;
import org.mathpiper.ui.gui.applications.voscilloscope.simulator.InterruptBitmap;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;
import java.util.EventObject;

/**
 * <p>Title: Virtual Oscilloscope.</p>
 * <p>Description: A Oscilloscope simulator</p>
 * <p>Copyright (C) 2003 José Manuel Gómez Soriano</p>
 * <h2>License</h2>
 * <p>
 This file is part of Virtual Oscilloscope.

 Virtual Oscilloscope is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Virtual Oscilloscope is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Virtual Oscilloscope; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 * </p>
 */

// Referenced classes of package es.upv.scope:
//            ButtonTime, PBackground

public class PTime extends Panel implements MouseListener
{

    public ButtonTime btnTime;
    public InterruptBitmap btnAtNormal;
    public RotateBitmap btnLevel;
    public InterruptBitmap btnPower;
    public BooleanBitmap btnLed;
    public RotateBitmap btnIntens;
    public RotateBitmap btnFocus;
    public RotateBitmap btnXPos;
    public InterruptBitmap btnXY;
    public RotateBitmap btnHoldOff;
    public InterruptBitmap btnMasMenos;
    public Scrollbar btnPosicion;
    Label lPosicion;

    public PTime()
    {
        setBackground(PBackground.background);
        setForeground(PBackground.panel);
        setLayout(null);
        constructElements();
    }

    public void iniciarElementos()
    {
        btnTime.setValue(0.2F);
        btnTime.setValueFino(0.0F);
        btnAtNormal.setValue(false);
        btnLevel.setValue(0.0F);
        btnPower.setValue(false);
        btnLed.setValue(btnPower.getValue());
        btnIntens.setValue(255F);
        btnFocus.setValue(2.0F);
        btnXPos.setValue(19F);
        btnXY.setValue(false);
        btnHoldOff.setValue(0.0F);
        btnMasMenos.setValue(false);
    }

    public void addNotify()
    {
        super.addNotify();
        posicionarElementos();
        iniciarElementos();
    }

    public void mouseClicked(MouseEvent e)
    {
        if(e.getSource() == btnPower)
        {
            btnLed.setValue(btnPower.getValue());
        }
    }

    public void mouseEntered(MouseEvent mouseevent)
    {
    }

    public void mouseExited(MouseEvent mouseevent)
    {
    }

    public void mousePressed(MouseEvent mouseevent)
    {
    }

    public void mouseReleased(MouseEvent mouseevent)
    {
    }

    private void constructElements()
    {
        String fino[] = {
            "bb00.gif", "bb01.gif", "bb02.gif", "bb03.gif", "bb04.gif", "bb05.gif", "bb06.gif", "bb07.gif", "bb08.gif", "bb09.gif",
            "bb10.gif", "bb11.gif", "bb12.gif", "bb13.gif", "bb14.gif", "bb15.gif", "bb16.gif", "bb17.gif", "bb18.gif", "bb19.gif",
            "bb20.gif", "bb21.gif", "bb22.gif", "bb23.gif", "bb24.gif", "bb25.gif", "bb26.gif", "bb27.gif", "bb28.gif", "bb29.gif",
            "bb31.gif", "bb32.gif", "bb33.gif", "bb34.gif", "bb35.gif", "bb36.gif", "bb37.gif", "bb38.gif", "bb39.gif", "bb40.gif",
            "bb41.gif", "bb42.gif", "bb43.gif", "bb44.gif", "bb45.gif"
        };
        String Tiempos[] = {
            "ba29.gif", "ba31.gif", "ba33.gif", "ba35.gif", "ba37.gif", "ba39.gif", "ba41.gif", "ba43.gif", "ba45.gif", "ba01.gif",
            "ba03.gif", "ba05.gif", "ba07.gif", "ba09.gif", "ba11.gif", "ba13.gif", "ba15.gif", "ba17.gif"
        };
        String pulsador[] = {
            "Bc01.gif", "Bc00.gif"
        };
        String Led[] = {
            "led1.gif", "led0.gif"
        };
        double ValuesTiempo[] = {
            5E-007F, 1E-006F, 2E-006F, 5E-006F, 1E-005F, 2E-005F, 5E-005F, 0.0001F, 0.0002F, 0.0005F,
            0.001F, 0.002F, 0.005F, 0.01F, 0.02F, 0.05F, 0.1F, 0.2F
        };
        double ValuesFinos[] = new double[46];
        for(int i = 0; i < 46; i++)
        {
            ValuesFinos[i] = (double)((double)i * 0.5D);
        }

        double ValuesIntens[] = new double[25];
        for(int i = 0; i < 25; i++)
        {
            ValuesIntens[i] = (i * 255) / 24;
        }

        double ValuesFocus[] = new double[18];
        for(int i = 0; i < 18; i++)
        {
            ValuesFocus[i] = i;
        }

        double ValuesXPos[] = new double[40];
        for(int i = 0; i < 40; i++)
        {
            ValuesXPos[i] = i - 20;
        }

        double ValuesHold[] = new double[46];
        for(int i = 0; i < 46; i++)
        {
            ValuesHold[i] = (i * 100) / 45;
        }

        double ValuesLevel[] = new double[93];
        for(int i = 0; i < 93; i++)
        {
            ValuesLevel[i] = (i * 100) / 46 - 100;
        }

        /*Message message = Message.makeDialog("              Panel 1 of 3. Loading button 1 of 12: button of POWER        " +
"     "
, "Information", false);*/
        btnPower = new InterruptBitmap(pulsador);
        add(btnPower);
        btnPower.putTitulo("POWER");
        btnPower.addMouseListener(this);
        //message.setText("Panel 1 of 3. Loading button 2 of 12: button of XY");
        btnXY = new InterruptBitmap(pulsador);
        add(btnXY);
        btnXY.putTitulo("XY");
        btnXY.addMouseListener(this);
        //message.setText("Panel 1 of 3. Loading button 3 of 12: button of AT/NORMAL");
        btnAtNormal = new InterruptBitmap(pulsador);
        add(btnAtNormal);
        btnAtNormal.putTitulo("At/Normal");
        btnAtNormal.addMouseListener(this);
        //message.setText("Panel 1 of 3. Loading button 4 of 12: button of +/-");
        btnMasMenos = new InterruptBitmap(pulsador);
        add(btnMasMenos);
        btnMasMenos.putTitulo("+/-");
        btnMasMenos.addMouseListener(this);
        //message.setText("Panel 1 of 3. Loading button 5 of 12: Luz of encendido");
        btnLed = new BooleanBitmap(Led);
        add(btnLed);
        btnLed.addMouseListener(this);
        //message.setText("Panel 1 of 3. Loading button 6 of 12: potenci\363metro of INTENSIDAD");
        btnIntens = new RotateBitmap(fino, ValuesIntens);
        add(btnIntens);
        btnIntens.putTitulo("Intensity");
        btnIntens.addMouseListener(this);
        //message.setText("Panel 1 of 3. Loading button 7 of 12: potenci\363metro of FOCO");
        btnFocus = new RotateBitmap(fino, ValuesFocus);
        add(btnFocus);
        btnFocus.putTitulo("Focus");
        btnFocus.addMouseListener(this);
        //message.setText("Panel 1 of 3. Loading button 8 of 12: potenci\363metro of X POS");
        btnXPos = new RotateBitmap(fino, ValuesXPos);
        add(btnXPos);
        btnXPos.putTitulo("XPos");
        btnXPos.addMouseListener(this);
        //message.setText("Panel 1 of 3. Loading button 9 of 12: potenci\363metro of HOLD OFF");
        btnHoldOff = new RotateBitmap(fino, ValuesHold);
        add(btnHoldOff);
        btnHoldOff.putTitulo("HoldOff");
        btnHoldOff.addMouseListener(this);
        //message.setText("Panel 1 of 3. Loading button 10 of 12: potenci\363metro of LEVEL");
        btnLevel = new RotateBitmap(fino, ValuesLevel);
        add(btnLevel);
        btnLevel.putTitulo("Level");
        btnLevel.addMouseListener(this);
        //message.setText("Panel 1 of 3. Loading button 11 of 12: potenci\363metro of TIME");
        btnTime = new ButtonTime(Tiempos, ValuesTiempo, fino, ValuesFinos);
        add(btnTime);
        btnTime.putTitulo("TIME");
        btnTime.addMouseListener(this);
        //message.setText("Panel 1 of 3. Loading button 12 of 12: barra of POSICI\323N");
        btnPosicion = new Scrollbar(0);
        add(btnPosicion);
        btnPosicion.setVisible(false);
        lPosicion = new Label("POSICI\323N");
        add(lPosicion);
        lPosicion.setVisible(false);
        lPosicion.setForeground(Color.orange);
        lPosicion.setBackground(PBackground.relleno);
        //message.closeDialog();
    }

    public void posicionarElementos()
    {
        btnPower.setLocation(8, 2);
        btnLed.setLocation(58, 21);
        btnXY.setLocation(88, 2);
        btnTime.setLocation(167, 2);
        btnAtNormal.setLocation(335, 2);
        btnIntens.setLocation(8, 38);
        btnFocus.setLocation(8, 104);
        btnXPos.setLocation(88, 38);
        btnHoldOff.setLocation(85, 104);
        btnLevel.setLocation(344, 51);
        btnMasMenos.setLocation(357, 128);
        btnPosicion.setBounds(142, 145, 240, 15);
        lPosicion.setLocation(77, 142);
        lPosicion.setSize(lPosicion.getMinimumSize());
    }

    public Dimension getMinimumSize()
    {
        return new Dimension(396, 168);
    }

    public Dimension getPreferredSize()
    {
        return getMinimumSize();
    }

    public void setLocation(int x, int y)
    {
        super.setLocation(x, y);
        setSize(getMinimumSize());
    }

    public void update(Graphics g)
    {
        paint(g);
    }

    public void paint(Graphics g)
    {
        drawBackground(g);
    }

    public void drawBackground(Graphics g)
    {
        g.setColor(PBackground.relleno);
        g.fillRoundRect(0, 0, 396, 168, 10, 10);
    }
}
