package org.mathpiper.ui.gui.applications.voscilloscope.oscilloscope;

import org.mathpiper.ui.gui.applications.voscilloscope.simulator.SuspendThread;
import java.awt.*;

/**
 * <p>Title: Virtual Oscilloscope.</p>
 * <p>Description: A Oscilloscope simulator</p>
 * <p>Copyright (C) 2003 José Manuel Gómez Soriano</p>
 * <h2>License</h2>
 * <p>
 This file is part of Virtual Oscilloscope.

 Virtual Oscilloscope is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Virtual Oscilloscope is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Virtual Oscilloscope; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.
 * </p>
 */

// Referenced classes of package es.upv.scope:
//            Signal, Screen

public class SignalManager extends SuspendThread
{

    public final double PI = 3.1415959999999998D;
    public final boolean AUTOMATIC = true;
    public final boolean NORMAL = false;
    public final boolean POS = false;
    public final boolean NEG = true;
    public Signal channel1;
    public Signal channel2;
    private int xPos;
    private double time;
    private boolean xy;
    private boolean atNormal;
    private int displayLevel;
    private boolean disPosNeg;
    private int posShow;
    private int holdOff;
    private boolean xMag;
    private boolean add;
    private boolean ch12;
    private int quality;
    private boolean digital;
    private Screen screen;
    private int oldX = -1;
    private int oldY1 = -1;

    public synchronized void rewind()
    {
        channel1.rewind();
        channel2.rewind();
    }

    public SignalManager(Screen pan)
    {
        //channel1 = new Signal();
        
        //channel1 = Signal.generateSquare(5, 0, 0, 6000, 100, 20, 0);
        
        channel1 = Signal.generateSinusodial(5, 0, 6000, 100, 20, 0);
        
        
        channel2 = new Signal();
        screen = pan;
    }

    public void run()
    {
        do
        {
            drawSignals();
            
            try
            {
                Thread.sleep(100);
            }
            catch(InterruptedException ie)
            {
            }
            
        } while(true);
    }

    public void drawSignals()
    {
        double pos1 = 0.0F;
        double pos2 = 0.0F;
        int width = screen.getSize().width;
        boolean disp = true;
        if(digital)
        {
            pos1 = posShow;
            pos2 = posShow;
        } else
        {
            avanzaHoldOff();
            disp = buscaPrimerValue();
        }
        if(xy)
        {
            screen.erase();
        }
        for(int x = xPos - 19; x < width; x++)
        {
            if(disp)
            {
                drawSignal(x);
            } else
            {
                screen.drawCol(x);
            }
            Thread.yield(); //todo:tk:
        }

        if(digital)
        {
            channel1.setPosShow(pos1);
            channel2.setPosShow(pos2);
        }
        
        oldX = -1;
    }

    public synchronized void drawSignal(int x)
    {
        int width = screen.getSize().width;
        int height = screen.getSize().height;
        for(int i = 0; i < quality; i++)
        {
            double value1 = channel1.sigSample(time / (double)((xMag ? 10 * width : width) * quality));
            double value2 = channel2.sigSample(time / (double)((xMag ? 10 * width : width) * quality));
            int y1;
            int y2;
            if(xy)
            {
                y1 = (int)((value1 * (double)width) / ((double)10 * channel1.getVDiv()));
                y2 = (int)((value2 * (double)height) / ((double)10 * channel2.getVDiv()));
                screen.putPixel(y1 + width / 2, y2, Color.yellow);
                continue;
            }
            y1 = (int)((value1 * (double)height) / ((double)10 * channel1.getVDiv()));
            y2 = (int)((value2 * (double)height) / ((double)10 * channel2.getVDiv()));
            screen.drawCol(x);
            if(add)
            {
                screen.putPixel(x, y1 + y2, Color.yellow);
                continue;
            }
            
            if(channel1.isVisible())
            {
                //screen.putPixel(x, y1, Color.yellow);
                
                //Experimental method for drawing lines between the points.
                if(oldX == -1)
                {
                    oldX = x;
                    oldY1 = y1;
                }
                screen.putLine(oldX, oldY1, x, y1, Color.yellow);
                oldX = x;
                oldY1 = y1;
                
            }
            
            
            if(channel2.isVisible())
            {
                screen.putPixel(x, y2, Color.cyan);
            }
        }

    }

    private synchronized void avanzaHoldOff()
    {
        int width = screen.getSize().width;
        channel1.advance(((double)holdOff * time * (double)width * channel1.getFastSampling()) / (double)10);
        channel2.advance(((double)holdOff * time * (double)width * channel2.getFastSampling()) / (double)10);
    }

    private synchronized boolean buscaPrimerValue()
    {
        int height = screen.getSize().height;
        Signal channel;
        if(ch12)
        {
            channel = channel2;
        } else
        {
            channel = channel1;
        }
        double level;
        if(!atNormal)
        {
            level = (channel.getVDiv() * (double)displayLevel) / (double)20;
        } else
        if(add)
        {
            level = channel1.getContinuous() + channel2.getContinuous();
        } else
        {
            level = channel.getContinuous();
        }
        int tamanyo;
        if(add)
        {
            tamanyo = channel1.getLength();
            if(channel2.getLength() > tamanyo)
            {
                tamanyo = channel2.getLength();
            }
            if(tamanyo == 0)
            {
                return true;
            }
            for(int i = tamanyo; i > 0; i--)
            {
                double value1ant = channel1.getVoltage((int)channel1.getPosShow());
                double value2ant = channel2.getVoltage((int)channel2.getPosShow());
                channel1.advance(1.0F);
                channel2.advance(1.0F);
                double value1 = channel1.getVoltage((int)channel1.getPosShow());
                double value2 = channel2.getVoltage((int)channel2.getPosShow());
                if(!disPosNeg && value1 + value2 <= value1ant + value2ant && value1ant + value2ant >= level && value1 + value2 <= level)
                {
                    return true;
                }
                if(disPosNeg && value1 + value2 >= value1ant + value2ant && value1ant + value2ant <= level && value1 + value2 >= level)
                {
                    return true;
                }
            }

            return false;
        }
        tamanyo = channel.getLength();
        if(tamanyo == 0)
        {
            return true;
        }
        for(int i = tamanyo; i > 0; i--)
        {
            double value1ant = channel.getVoltage((int)channel.getPosShow());
            channel1.advance(1.0F);
            channel2.advance(1.0F);
            double value1 = channel.getVoltage((int)channel.getPosShow());
            if(!disPosNeg && value1 <= value1ant && value1ant >= level && value1 <= level)
            {
                return true;
            }
            if(disPosNeg && value1 >= value1ant && value1ant <= level && value1 >= level)
            {
                return true;
            }
        }

        return false;
    }

    public synchronized boolean isDigital()
    {
        return digital;
    }

    public synchronized void setXPos(int pos)
    {
        xPos = pos;
    }

    public synchronized int getXPos()
    {
        return xPos;
    }

    public synchronized void setTime(double time)
    {
        this.time = time;
    }

    public synchronized double getTime()
    {
        return time;
    }

    public synchronized void setXY(boolean xy)
    {
        if(this.xy != xy)
        {
            rewind();
            this.xy = xy;
        }
    }

    public synchronized boolean isXY()
    {
        return xy;
    }

    public synchronized void setAtNormal(boolean atnormal)
    {
        atNormal = atnormal;
    }

    public synchronized boolean getAtNormal()
    {
        return atNormal;
    }

    public synchronized void setLevelDisparo(int level)
    {
        displayLevel = level;
    }

    public synchronized int getLevelDisparo()
    {
        return displayLevel;
    }

    public synchronized void setDisPosNeg(boolean disparo)
    {
        disPosNeg = disparo;
    }

    public synchronized boolean getDisPosNeg()
    {
        return disPosNeg;
    }

    public synchronized void setHoldOff(int hold)
    {
        holdOff = hold;
    }

    public synchronized int getHoldOff()
    {
        return holdOff;
    }

    public synchronized void setXMag(boolean xmag)
    {
        xMag = xmag;
    }

    public synchronized boolean isXMag()
    {
        return xMag;
    }

    public synchronized void setAdd(boolean add)
    {
        if(this.add != add)
        {
            this.add = add;
            rewind();
        }
    }

    public synchronized boolean isAdd()
    {
        return add;
    }

    public synchronized boolean isDual()
    {
        return channel1.isVisible() && channel2.isVisible();
    }

    public synchronized void setQuality(int quality)
    {
        this.quality = quality;
    }

    public synchronized int getQuality()
    {
        return quality;
    }

    public synchronized void setCH12(boolean ch12)
    {
        this.ch12 = ch12;
    }

    public synchronized boolean getCH12()
    {
        return ch12;
    }

    public synchronized void setDigital(boolean dig)
    {
        digital = dig;
    }

    public synchronized void setPosShow(int pos)
    {
        posShow = pos;
    }
}
