package org.mathpiper.ui.gui.applications.circuitpiper.model.components.active;

import java.awt.Color;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.Component;
import static org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.CurrentSource.componentCounter;
import org.mathpiper.ui.gui.applications.circuitpiper.view.DrawingPanel;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ScaledGraphics;

public class VCVS extends Component {
    
    public VCVS(int x, int y, String uid, DrawingPanel drawingPanel) {
        super(x, y, drawingPanel);
        if(uid == null)
        {
            componentUID = componentCounter++ + "";
        }
        else
        {
            componentUID = uid;
            
            try{
                int number = Integer.parseInt(uid);
        
                if(number >= componentCounter)
                {
                    componentCounter = number + 1;
                }
            }
            catch (NumberFormatException nfe)
            {
            }
        }
        init();
        //primaryValue = .0345;
        //enteredPrimaryValue = "34.5";
    }
    
    

    public void init() {
        primary = "Current";
        primaryUnit = "Amp";
        primaryUnitPlural = "Amps";
        primarySymbol = "I";
        primaryUnitSymbol = "A";
        preselectedPrimaryPrefix = "mA";
    }    
    
    
    public void draw(ScaledGraphics sg) {
        super.draw(sg);
        
        int headTerminalX = headTerminal.getX();
        int headTerminalY = headTerminal.getY();
        int tailTerminalX = tailTerminal.getX();
        int tailTerminalY = tailTerminal.getY();
        
        int rise = tailTerminalY - headTerminalY;
        int run = tailTerminalX - headTerminalX;
        
        double radius = 14;
        double hypotenuse = Math.sqrt(rise * rise + run * run); // Distance between terminals.
        double centerX = (headTerminalX + tailTerminalX) / 2;
        double centerY = (headTerminalY + tailTerminalY) / 2;
        double sine = run / hypotenuse;
        double cosine = rise / hypotenuse;
        double headX = headTerminalX + run / 2.0 - radius * sine;
        double headY = headTerminalY + rise / 2.0 - radius * cosine;
        double tailX = tailTerminalX - run / 2.0 + radius * sine;
        double tailY = tailTerminalY - rise / 2.0 + radius * cosine;
        
        if (hypotenuse >= 2 * radius) {
            sg.drawLine(headTerminalX, headTerminalY, headX, headY);
            sg.drawLine(tailX, tailY, tailTerminalX, tailTerminalY);
            
           
            sg.drawLine(headX,  headY, 
                    centerX + radius * cosine,
                    centerY - radius * sine);
            
            sg.drawLine(headX,  headY, 
                    centerX - radius * cosine,
                    centerY + radius * sine);
            
            sg.drawLine(tailX,  tailY, 
                    centerX + radius * cosine,
                    centerY - radius * sine);
            
            sg.drawLine(tailX,  tailY, 
                    centerX - radius * cosine,
                    centerY + radius * sine);            
        } else {
            
        }
        if (hypotenuse > 0) {
            double shaftScale = 9.0;
            double shaftScale2 = 5.0;
            double shaftScale3 = 7.0;
            double setback = 2.0;
            
            double oldLineWidth = sg.getLineWidth();
            sg.setLineWidth(3);
            
            // + at top.
            sg.drawLine(centerX - shaftScale2 * sine,
                     centerY - shaftScale2 * cosine,
                     centerX - shaftScale * sine,
                     centerY - shaftScale * cosine);
            
            sg.drawLine(centerX - (setback * cosine) - shaftScale3 * sine, 
                    centerY + (setback * sine) - shaftScale3 * cosine, 
                     centerX + (setback * cosine) - shaftScale3 * sine,
                     centerY - (setback * sine) - shaftScale3 * cosine);
            
            
            // - at bottom.
            sg.drawLine(centerX - (setback * cosine) + shaftScale3 * sine, 
                    centerY + (setback * sine) + shaftScale3 * cosine, 
                     centerX + (setback * cosine) + shaftScale3 * sine,
                     centerY - (setback * sine) + shaftScale3 * cosine);
            
            sg.setLineWidth(oldLineWidth);
        }
    }
    
    public String toString()
    {
        return super.toString();
    }
}
