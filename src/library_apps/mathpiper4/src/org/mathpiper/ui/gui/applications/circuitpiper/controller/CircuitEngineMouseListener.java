package org.mathpiper.ui.gui.applications.circuitpiper.controller;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.Iterator;
import javax.swing.JDialog;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTextArea;
import javax.swing.event.MouseInputListener;

import org.mathpiper.ui.gui.applications.circuitpiper.model.Terminal;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.ACCurrentSource;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.ACVoltageSource;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.Ammeter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.Block;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.CapacitanceMeter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.Capacitor;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.CurrentIntegrator;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.CurrentSource;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.Component;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.InductanceMeter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.Inductor;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.Ohmmeter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.Resistor;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.VoltageIntegrator;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.VoltageSource;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.Voltmeter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.Wire;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ChangeACParametersFrame;
import org.mathpiper.ui.gui.applications.circuitpiper.view.ChangePrimaryValueFrame;
import org.mathpiper.ui.gui.applications.circuitpiper.view.CircuitEnginePanel;
import org.mathpiper.ui.gui.applications.circuitpiper.view.GraphFrame;
import org.mathpiper.ui.gui.applications.circuitpiper.view.PhasePlane;

/*AUTHORS:

 - Kevin Stueve (2009-12-20): initial published version
 #*****************************************************************************
 #       Copyright (C) 2009 Kevin Stueve kstueve@uw.edu
 #
 #  Distributed under the terms of the GNU General Public License (GPL)
 #                  http://www.gnu.org/licenses/
 #*****************************************************************************
 */
public final class CircuitEngineMouseListener implements MouseMotionListener, MouseInputListener {

    CircuitEnginePanel myParentCircuitEnginePanel;

    public CircuitEngineMouseListener(final CircuitEnginePanel theParentCircuitEnginePanel) {
        myParentCircuitEnginePanel = theParentCircuitEnginePanel;
    }

    //@Override
    public void mouseDragged(MouseEvent theMouseEvent) {
        //System.out.println("dragged");
        myParentCircuitEnginePanel.waiting = true;
        synchronized (myParentCircuitEnginePanel.drawingPanel) {
            //System.out.println("begin drag");
            // TODO make more elegant
            myParentCircuitEnginePanel.setMouseX(theMouseEvent.getX());
            myParentCircuitEnginePanel.setMouseY(theMouseEvent.getY());
            if (!myParentCircuitEnginePanel.isDrawing()
                    && myParentCircuitEnginePanel.isMovingPoint()) {
                handleDragPoint();
            } else if (!myParentCircuitEnginePanel.isDrawing()
                    && myParentCircuitEnginePanel.isMovingComponent()) {
                handleDragComponent();
            }
            //System.out.println("end dragged");
        }
        myParentCircuitEnginePanel.waiting = false;
    }

    public void determineHint() {
        if (!myParentCircuitEnginePanel.isDrawing()
                && !myParentCircuitEnginePanel.isMovingPoint()) {
            if (myParentCircuitEnginePanel.nearTerminal()) {
                myParentCircuitEnginePanel.hintNearTerminal();
                myParentCircuitEnginePanel.myNearestComponent = null;
                // myParentCircuitEnginePanel.myNearestSwitch = null;
            } else if (myParentCircuitEnginePanel.nearSwitch()) {
                myParentCircuitEnginePanel.hintNearSwitch();
                myParentCircuitEnginePanel.myNearestComponent = null;
            } else if (myParentCircuitEnginePanel.nearComponent()) {
                myParentCircuitEnginePanel.hintNearComponent();
                // myParentCircuitEnginePanel.myNearestTerminal=null;
            } else {
                myParentCircuitEnginePanel.myNearestComponent = null;
                myParentCircuitEnginePanel.myNearestTerminal = null;
                myParentCircuitEnginePanel.setHintStarting();
            }
        }
    }

    //@Override
    public void mouseMoved(MouseEvent theMouseEvent) {
 //System.out.println("moved");

        //System.out.println("waiting");
        myParentCircuitEnginePanel.waiting = true;

        synchronized (myParentCircuitEnginePanel.drawingPanel) {
            // System.out.println("moved");
            myParentCircuitEnginePanel.setMouseX(theMouseEvent.getX());
            myParentCircuitEnginePanel.setMouseY(theMouseEvent.getY());
            if (myParentCircuitEnginePanel.isDrawing()) {
                myParentCircuitEnginePanel.myTempComponent
                        .moveTail(myParentCircuitEnginePanel.nearestTerminalXPixels(), myParentCircuitEnginePanel
                                .nearestTerminalYPixels());
            } else {
                determineHint();
            }
            myParentCircuitEnginePanel.repaint();
        }
        myParentCircuitEnginePanel.waiting = false;
    }

    //@Override
    public void mouseClicked(MouseEvent theMouseEvent) {
        /*
         if (theMouseEvent.getButton() == MouseEvent.BUTTON3) {
         JPopupMenu popup = new JPopupMenu();
         JMenuItem menuItem = new JMenuItem("Save image to file");
         menuItem.addActionListener(new ActionListener() {

         public void actionPerformed(ActionEvent e) {
         Utility.saveImageOfComponent(myParentCircuitEnginePanel.drawingPanel);
         }

         });

         popup.add(menuItem);
         popup.show(myParentCircuitEnginePanel, 10, 10);
         }
         */
        myParentCircuitEnginePanel.waiting = true;

        synchronized (myParentCircuitEnginePanel.drawingPanel) {
            //System.out.println("clicked");
            // TODO reduce redundancy
            if (theMouseEvent.getClickCount() == 1 && theMouseEvent.getButton() == MouseEvent.BUTTON1) {
                if (myParentCircuitEnginePanel.isMovingPoint()) {
                    // todo:tk:Ignore clicks if the mouse is moving.
                    //myParentCircuitEnginePanel.setIsMovingPoint(false);
                }

                if (!myParentCircuitEnginePanel.isDrawing()
                        && !myParentCircuitEnginePanel.isMovingPoint()
                        && !myParentCircuitEnginePanel.isMovingComponent()) {
                    // todo:tk:legitimate first click in drawing mode.

                    if (!myParentCircuitEnginePanel.nearTerminal() && myParentCircuitEnginePanel.nearSwitch()) {
                        // todo:tk:if the intent is to flip a switch, then leave.

                        //myParentCircuitEnginePanel.myNearestSwitch.flip();
                        //myParentCircuitEnginePanel.repaint();
                        return;
                    }
                    myParentCircuitEnginePanel.myNearestComponent = null;
                    myParentCircuitEnginePanel.myNearestTerminal = null;

                    // clicked
                    myParentCircuitEnginePanel.setIsDrawing(true);

                    myParentCircuitEnginePanel.setHintDrawing();

                    try {
                        myParentCircuitEnginePanel.newTempComponent(
                                myParentCircuitEnginePanel.mySelectedComponent.replaceAll(" ", ""),
                                myParentCircuitEnginePanel.nearestTerminalXPixels(),
                                myParentCircuitEnginePanel.nearestTerminalYPixels());

                        myParentCircuitEnginePanel.repaint();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    // Click another point on the canvas to create a component, or click
                    // cancel
                    // g.fillOval((int)(x1+alpha*deltax-4),(int)(y1+beta*deltay-4), 8, 8);
                } else if (myParentCircuitEnginePanel.isDrawing()) {
                    // todo:tk:second click, the tail of a component is being placed.

                    if (myParentCircuitEnginePanel.myTempComponent.badSize()) {
                        return;
                    }

                    Point headPoint = myParentCircuitEnginePanel.myTempComponent.headTerminal.getPosition();

                    Point nearestPoint = new Point(myParentCircuitEnginePanel.nearestTerminalXPixels(),
                            myParentCircuitEnginePanel.nearestTerminalYPixels());

                    if (myParentCircuitEnginePanel.myCircuit.myTerminals.containsKey(nearestPoint)
                            && myParentCircuitEnginePanel.myCircuit.myTerminals.containsKey(headPoint)
                            && myParentCircuitEnginePanel.myCircuit.myTerminals.get(headPoint)
                            .isConnectedTo(myParentCircuitEnginePanel.myCircuit.myTerminals.get(nearestPoint))) {
                        return;

                    }
                    /*myParentCircuitEnginePanel.nearTerminal();//required-side effects
                     if (myParentCircuitEnginePanel.nearTerminal()&&//##removed dec 23 08
            
                     myParentCircuitEnginePanel.myGridPoints.containsKey(p)&&
                     myParentCircuitEnginePanel.myGridPoints.get(p)
                     .isConnectedTo(myParentCircuitEnginePanel.myNearestTerminal)
                     ){
                     return;
                     }*/
                    /*if (myParentCircuitEnginePanel.nearTerminal()
                     &&!(myParentCircuitEnginePanel.myGridPoints.containsKey(p)&&
                     myParentCircuitEnginePanel.myGridPoints.get(p)
                     .isConnectedTo(myParentCircuitEnginePanel.myNearestTerminal))){
                     myParentCircuitEnginePanel.setIsDrawing(false);
                     myParentCircuitEnginePanel.setHintStarting();
                     myParentCircuitEnginePanel.addElectricComponent();
                     }*/
                    myParentCircuitEnginePanel.setIsDrawing(false);
                    //myParentCircuitEnginePanel.setHintStarting();
                    myParentCircuitEnginePanel.addElectricComponent(myParentCircuitEnginePanel.myTempComponent, myParentCircuitEnginePanel.nearestTerminalXPixels(), myParentCircuitEnginePanel.nearestTerminalYPixels());
                    determineHint();
                    myParentCircuitEnginePanel.repaint();
                }
            } else if (theMouseEvent.getClickCount() == 1 && theMouseEvent.getButton() == MouseEvent.BUTTON3) {
                //System.out.println("hello");
                if (!myParentCircuitEnginePanel.isDrawing()
                        && !myParentCircuitEnginePanel.isMovingPoint()
                        && !myParentCircuitEnginePanel.isMovingComponent()) {
                    if (myParentCircuitEnginePanel.nearTerminal()) {
                        final Point point = myParentCircuitEnginePanel.myNearestTerminal.getPosition();
                        JPopupMenu jPopupMenu = new JPopupMenu();
                        JMenuItem deleteMenuItem = new JMenuItem("Delete Terminal");
                        deleteMenuItem.addActionListener(new ActionListener() {
                            public void actionPerformed(final ActionEvent e) {
                                //System.out.println("delete");
                                Iterator<Component> i = myParentCircuitEnginePanel.myCircuit.electricComponents.iterator();
                                while (i.hasNext()) {
                                    Component ec = i.next();
                                    if (ec.headTerminal.getPosition().equals(point)) {
                                        ec.disconnectHeadAndTail();
                                        i.remove();//myParentCircuitEnginePanel.myCircuit.electricComponents.remove(ec);
                                        if (ec.tailTerminal.myConnectedTo.size() == 0) {
                                            myParentCircuitEnginePanel.myCircuit.myTerminals.remove(ec.tailTerminal.getPosition());
                                        }
                                    } else if (ec.tailTerminal.getPosition().equals(point)) {
                                        ec.disconnectHeadAndTail();
                                        i.remove();//myParentCircuitEnginePanel.myCircuit.electricComponents.remove(ec);
                                        if (ec.headTerminal.myConnectedTo.size() == 0) {
                                            myParentCircuitEnginePanel.myCircuit.myTerminals.remove(ec.headTerminal.getPosition());
                                        }
                                    }
                                }
                                myParentCircuitEnginePanel.myCircuit.myTerminals.remove(point);
                                // jPopupMenu.setVisible(false);
                            }
                        });
                        jPopupMenu.add(deleteMenuItem);
                        jPopupMenu.show(myParentCircuitEnginePanel, myParentCircuitEnginePanel.getMouseX(),
                                myParentCircuitEnginePanel.getMouseY());
                    } else if (myParentCircuitEnginePanel.nearComponent()) {
                        final Component ec = myParentCircuitEnginePanel.myNearestComponent;
                        JPopupMenu jPopupMenu = new JPopupMenu();
                        
                        if (ec.getClass() == Voltmeter.class || ec.getClass() == Ammeter.class
                                || ec.getClass() == CurrentIntegrator.class || ec.getClass() == VoltageIntegrator.class || ec.getClass() == Ohmmeter.class
                                || ec.getClass() == CapacitanceMeter.class || ec.getClass() == InductanceMeter.class) {
                            JMenuItem graphItem = new JMenuItem("Graph this " + ec.primary.substring(0, 1).toUpperCase() + ec.primary.substring(1));
                            graphItem.addActionListener(new ActionListener() {
                                public void actionPerformed(final ActionEvent e) {

                                    GraphFrame gf = new GraphFrame(myParentCircuitEnginePanel, 0, 0, ec.primary, ec.primaryUnitSymbol);
                                    ec.graphFrame = gf;
                                    //JOptionPane.showMessageDialog(null, "Not available in demo");
                                }
                            });
                            jPopupMenu.add(graphItem);

                            JMenuItem phasePlaneYItem = new JMenuItem("Make this " + ec.primary.substring(0, 1).toUpperCase()
                                    + ec.primary.substring(1) + " the Y-axis of a phase plane");
                            phasePlaneYItem.addActionListener(new ActionListener() {
                                public void actionPerformed(final ActionEvent e) {
                                    // /*demo  
                                    if (myParentCircuitEnginePanel.phasePlanes.size() == 0
                                            || (myParentCircuitEnginePanel.phasePlanes.getLast().yComponent != null
                                            && myParentCircuitEnginePanel.phasePlanes.getLast().xComponent != null)) {
                                        PhasePlane phasePlane = new PhasePlane(myParentCircuitEnginePanel, 0, 0);
                                        phasePlane.setY(ec);
                                        myParentCircuitEnginePanel.phasePlanes.add(phasePlane);
                                    } else {//if (myParentCircuitEnginePanel.phasePlanes.getLast().xComponent==null){
                                        myParentCircuitEnginePanel.phasePlanes.getLast().setY(ec);
                                    }
                                    // */
                                    //      JOptionPane.showMessageDialog(null, "Not available in demo");
                                }
                            });
                            jPopupMenu.add(phasePlaneYItem);

                            JMenuItem phasePlaneXItem = new JMenuItem("Make this " + ec.primary.substring(0, 1).toUpperCase()
                                    + ec.primary.substring(1) + " the X-axis of a phase plane");
                            phasePlaneXItem.addActionListener(new ActionListener() {
                                public void actionPerformed(final ActionEvent e) {
                                    // /*demo   
                                    if (myParentCircuitEnginePanel.phasePlanes.size() == 0
                                            || (myParentCircuitEnginePanel.phasePlanes.getLast().yComponent != null
                                            && myParentCircuitEnginePanel.phasePlanes.getLast().xComponent != null)) {
                                        PhasePlane phasePlane = new PhasePlane(myParentCircuitEnginePanel, 0, 0);
                                        phasePlane.setX(ec);
                                        myParentCircuitEnginePanel.phasePlanes.add(phasePlane);
                                    } else {//if (myParentCircuitEnginePanel.phasePlanes.getLast().yComponent==null){
                                        myParentCircuitEnginePanel.phasePlanes.getLast().setX(ec);
                                    }
                                    //  */
                                    //   JOptionPane.showMessageDialog(null, "Not available in demo");
                                }
                            });
                            jPopupMenu.add(phasePlaneXItem);
                        }
                        
                        if (ec.getClass() == VoltageSource.class || ec.getClass() == Capacitor.class || ec.getClass() == Resistor.class
                                || ec.getClass() == CurrentSource.class || ec.getClass() == Inductor.class) {
                            JMenuItem change = new JMenuItem("Change " + ec.primary);
                            change.addActionListener(new ActionListener() {
                                public void actionPerformed(final ActionEvent e) {

                                    new ChangePrimaryValueFrame(myParentCircuitEnginePanel, 0, 0, ec);
                                    //JOptionPane.showMessageDialog(null, "Not available in demo");
                                }
                            });
                            jPopupMenu.add(change);
                        }
                        
                        if (ec.getClass() == ACVoltageSource.class || ec.getClass() == ACCurrentSource.class) {
                            JMenuItem change = new JMenuItem("Adjust Paramaters");
                            change.addActionListener(new ActionListener() {
                                public void actionPerformed(final ActionEvent e) {

                                    new ChangeACParametersFrame(myParentCircuitEnginePanel, 0, 0, ec);
                                    //JOptionPane.showMessageDialog(null, "Not available in demo");
                                }
                            });
                            jPopupMenu.add(change);
                        }
                        
                        if (ec.getClass() == CurrentIntegrator.class || ec.getClass() == Capacitor.class
                                || ec.getClass() == Inductor.class || ec.getClass() == VoltageIntegrator.class) {
                            JMenuItem resetMenuItem = new JMenuItem("Reset " + ec.secondary + " to 0 " + ec.secondaryUnitSymbol);
                            resetMenuItem.addActionListener(new ActionListener() {
                                public void actionPerformed(final ActionEvent e) {
                                    ec.secondaryValue = 0;
                                    if (ec.getClass() == CurrentIntegrator.class) {
                                        CurrentIntegrator ci = (CurrentIntegrator) ec;
                                        ci.chargeString = "0.00 C";
                                        myParentCircuitEnginePanel.repaint();
                                    }
                                    if (ec.getClass() == VoltageIntegrator.class) {
                                        VoltageIntegrator ci = (VoltageIntegrator) ec;
                                        ci.magneticFluxString = "0.00 Wb";
                                        myParentCircuitEnginePanel.repaint();
                                    }
                                }
                            });
                            jPopupMenu.add(resetMenuItem);
                        }
                        
                        if (ec.getClass() != Wire.class && ec.getClass() != Resistor.class) {
                            JMenuItem reverseOrientationMenuItem = new JMenuItem("Reverse Orientation");
                            reverseOrientationMenuItem.addActionListener(new ActionListener() {
                                public void actionPerformed(final ActionEvent e) {
                                    ec.reverse();
                                }
                            });
                            jPopupMenu.add(reverseOrientationMenuItem);
                        }
                        
                        if (ec.getClass() == Block.class) {
                            JMenuItem enterTextMenuItem = new JMenuItem("Text");
                            enterTextMenuItem.addActionListener(new ActionListener() {
                                public void actionPerformed(final ActionEvent e) {
                                    final JTextArea textArea = new JTextArea(4, 10);

                                    JOptionPane pane = new JOptionPane(textArea, JOptionPane.QUESTION_MESSAGE, JOptionPane.OK_CANCEL_OPTION) {
                                        @Override
                                        public void selectInitialValue() {
                                            textArea.requestFocusInWindow();
                                        }
                                    };
                                    JDialog dialog = pane.createDialog(null, "Enter text");
                                    dialog.setVisible(true);
                                    dialog.dispose();
                                    
                                    Object object = pane.getValue();
                                    if(object instanceof Integer)
                                    {
                                        if(((Integer) object) == JOptionPane.OK_OPTION)
                                        {

                                            Block block = (Block) ec;
                                            block.setText(textArea.getText());
                                        }
                                    }
                                }
                            });
                            jPopupMenu.add(enterTextMenuItem);
                        }

                        
                        
                        JMenuItem deleteMenuItem = new JMenuItem("Delete Component");
                        deleteMenuItem.addActionListener(new ActionListener() {
                            public void actionPerformed(final ActionEvent e) {
                                //System.out.println("delete");
                                ec.disconnectHeadAndTail();
                                if (ec.headTerminal.myConnectedTo.size() == 0) {
                                    myParentCircuitEnginePanel.myCircuit.myTerminals.remove(ec.headTerminal.getPosition());
                                }
                                if (ec.tailTerminal.myConnectedTo.size() == 0) {
                                    myParentCircuitEnginePanel.myCircuit.myTerminals.remove(ec.tailTerminal.getPosition());
                                }
                                myParentCircuitEnginePanel.myCircuit.electricComponents.remove(ec);
                                // jPopupMenu.setVisible(false);
                            }
                        });
                        jPopupMenu.add(deleteMenuItem);
                        
                        
                        // ===============================
                        jPopupMenu.show(myParentCircuitEnginePanel, myParentCircuitEnginePanel.getMouseX(),
                                myParentCircuitEnginePanel.getMouseY());
                    }
                }
            }
        }

        myParentCircuitEnginePanel.waiting = false;

    }

    //@Override
    public void mouseEntered(MouseEvent theMouseEvent) {
        // System.out.println("entered");
        myParentCircuitEnginePanel.setMouseEntered(true);
        myParentCircuitEnginePanel.repaint();
    }

    //@Override
    public void mouseExited(MouseEvent arg0) {
        myParentCircuitEnginePanel.setMouseEntered(false);
        if (!myParentCircuitEnginePanel.isDrawing()
                && !myParentCircuitEnginePanel.isMovingPoint()
                && !myParentCircuitEnginePanel.isMovingComponent()) {
            myParentCircuitEnginePanel.myNearestComponent = null;
            myParentCircuitEnginePanel.myNearestTerminal = null;
            myParentCircuitEnginePanel.setHintStarting();
        }
        myParentCircuitEnginePanel.repaint();
        // System.out.println("exited");
    }

    //@Override
    public void mousePressed(MouseEvent theMouseEvent) {

        myParentCircuitEnginePanel.waiting = true;

        synchronized (myParentCircuitEnginePanel.drawingPanel) {
            //System.out.println("pressed");
            if (theMouseEvent.getButton() == MouseEvent.BUTTON1) {
                myParentCircuitEnginePanel.setButtonState(true);
                if (!myParentCircuitEnginePanel.isDrawing()
                        && !myParentCircuitEnginePanel.isMovingPoint()
                        && !myParentCircuitEnginePanel.isMovingComponent()) {
                    if (!myParentCircuitEnginePanel.nearTerminal() && myParentCircuitEnginePanel.nearSwitch()) {
                        myParentCircuitEnginePanel.myNearestSwitch.flip();
                        myParentCircuitEnginePanel.repaint();
                    } else if (myParentCircuitEnginePanel.nearTerminal()) {
                        myParentCircuitEnginePanel.setIsMovingPoint(true);
                        myParentCircuitEnginePanel.myDraggedTerminal
                                = myParentCircuitEnginePanel.myNearestTerminal;
                    } else if (myParentCircuitEnginePanel.nearComponent()) {
                        //myParentCircuitEnginePanel.nearTerminal();//to set Nearest
                        myParentCircuitEnginePanel.setIsMovingComponent(true);
                        myParentCircuitEnginePanel.myDraggedComponent
                                = myParentCircuitEnginePanel.myNearestComponent;
                        myParentCircuitEnginePanel.myDraggedTerminal
                                = myParentCircuitEnginePanel.myNearestTerminal;
                    }
                }
            }
        }
        myParentCircuitEnginePanel.waiting = false;

    }

    //@Override
    public void mouseReleased(MouseEvent theMouseEvent) {

        myParentCircuitEnginePanel.waiting = true;

        synchronized (myParentCircuitEnginePanel.drawingPanel) {
            if (theMouseEvent.getButton() == MouseEvent.BUTTON1) {
                myParentCircuitEnginePanel.setButtonState(false);
                if (myParentCircuitEnginePanel.isMovingPoint()) {
                    //System.out.println("a"+myParentCircuitEnginePanel.myDraggedTerminal.myConnectedTo.size());
                    Point point
                            = //new Point(myParentCircuitEnginePanel.nearestX(), myParentCircuitEnginePanel.nearestY());
                            new Point(myParentCircuitEnginePanel.myDraggedTerminal.getX(),
                                    myParentCircuitEnginePanel.myDraggedTerminal.getY());
                    //System.out.println("aa"+myParentCircuitEnginePanel.myGridPoints.get(point).myConnectedTo.size());
                    for (Component e : myParentCircuitEnginePanel.myCircuit.electricComponents) {
                        if (e.headTerminal == myParentCircuitEnginePanel.myDraggedTerminal) {
                            e.setHead(myParentCircuitEnginePanel.myCircuit.myTerminals.get(point));
                            //System.out.println("head");
                        } else if (e.tailTerminal == myParentCircuitEnginePanel.myDraggedTerminal) {
                            e.setTail(myParentCircuitEnginePanel.myCircuit.myTerminals.get(point));
                            //System.out.println("tail");
                        }
                    }
                    //System.out.println(myParentCircuitEnginePanel.myDraggedTerminal.myConnectedTo.size());
                    myParentCircuitEnginePanel.myDraggedTerminal = null;
                    myParentCircuitEnginePanel.myDraggedComponent = null;
                    myParentCircuitEnginePanel.setIsMovingPoint(false);
                    myParentCircuitEnginePanel.setIsMovingComponent(false);
                } else if (myParentCircuitEnginePanel.isMovingComponent()) {
                    myParentCircuitEnginePanel.myDraggedTerminal = null;
                    myParentCircuitEnginePanel.myDraggedComponent = null;
                    myParentCircuitEnginePanel.setIsMovingPoint(false);
                    myParentCircuitEnginePanel.setIsMovingComponent(false);
                }
                determineHint();
                myParentCircuitEnginePanel.repaint();
            }
            //System.out.println("released");
        }

        myParentCircuitEnginePanel.waiting = false;

    }

    public void handleDragPoint() {
        if (myParentCircuitEnginePanel.nearestTerminalXPixels() != myParentCircuitEnginePanel.myDraggedTerminal.getX()
                || myParentCircuitEnginePanel.nearestTerminalYPixels() != myParentCircuitEnginePanel.myDraggedTerminal.getY()) {
            Point point = new Point(myParentCircuitEnginePanel.nearestTerminalXPixels(),
                    myParentCircuitEnginePanel.nearestTerminalYPixels());
            Point oldPoint = new Point(myParentCircuitEnginePanel.myDraggedTerminal.getX(),
                    myParentCircuitEnginePanel.myDraggedTerminal.getY());
            //System.out.println(point+" "+oldPoint);
            if (!myParentCircuitEnginePanel.myCircuit.myTerminals.containsKey(point)) {//that is if we are dragging to an unoccupied location
                if (myParentCircuitEnginePanel.myCircuit.myTerminals.get(oldPoint)
                        == myParentCircuitEnginePanel.myDraggedTerminal) {//that is, if there is no point left behind
                    //System.out.println("123");
                    myParentCircuitEnginePanel.myCircuit.myTerminals.remove(oldPoint);
                }
                //System.out.println("567");
                myParentCircuitEnginePanel.myCircuit.myTerminals
                        .put(point, myParentCircuitEnginePanel.myDraggedTerminal);
                myParentCircuitEnginePanel.myDraggedTerminal.setX(myParentCircuitEnginePanel
                        .nearestTerminalXPixels());
                myParentCircuitEnginePanel.myDraggedTerminal.setY(myParentCircuitEnginePanel
                        .nearestTerminalYPixels());
            } else {//if there is already a point here
                if (!myParentCircuitEnginePanel.myDraggedTerminal
                        .isConnectedTo(myParentCircuitEnginePanel.myCircuit.myTerminals.get(point))
                        && !myParentCircuitEnginePanel.myDraggedTerminal
                        .isConnectedToOrder2(myParentCircuitEnginePanel.myCircuit.myTerminals.get(point))) {//that is, if the new point at the new location isn't connectod to the old point
                    // System.out.println("not connected");
                    if (myParentCircuitEnginePanel.myCircuit.myTerminals.get(oldPoint)
                            == myParentCircuitEnginePanel.myDraggedTerminal) {//that is, if there is no point left behind after drag
                        //myParentCircuitEnginePanel.myDraggedTerminal=
                        //System.out.println("removed old");
                        myParentCircuitEnginePanel.myCircuit.myTerminals.remove(oldPoint);
                    }
                    myParentCircuitEnginePanel.myDraggedTerminal.setX(myParentCircuitEnginePanel
                            .nearestTerminalXPixels());
                    myParentCircuitEnginePanel.myDraggedTerminal.setY(myParentCircuitEnginePanel
                            .nearestTerminalYPixels());
                }
            }
            myParentCircuitEnginePanel.repaint();
        }
    }

    public void handleDragComponent() {
        if (myParentCircuitEnginePanel.nearestTerminalXPixels() != myParentCircuitEnginePanel.myDraggedTerminal.getX()
                || myParentCircuitEnginePanel.nearestTerminalYPixels() != myParentCircuitEnginePanel.myDraggedTerminal.getY()) {
            //System.out.println("dragged comp");
            Point point = new Point(myParentCircuitEnginePanel.nearestTerminalXPixels(),
                    myParentCircuitEnginePanel.nearestTerminalYPixels());
            Point oldPoint = new Point(myParentCircuitEnginePanel.myDraggedTerminal.getX(),
                    myParentCircuitEnginePanel.myDraggedTerminal.getY());
            Terminal newTerminal = new Terminal(myParentCircuitEnginePanel.nearestTerminalXPixels(),
                    myParentCircuitEnginePanel.nearestTerminalYPixels());
            if (myParentCircuitEnginePanel.nearTerminal()) {//must be called because this method has side effects
                //System.out.println("connected");
            }
            if (//myParentCircuitEnginePanel.myGridPoints.get(oldPoint)
                    //== myParentCircuitEnginePanel.myDraggedTerminal&&//if no point would be left behind
                    myParentCircuitEnginePanel.myDraggedTerminal.myConnectedTo.size() == 1
                    && !(myParentCircuitEnginePanel.myCircuit.myTerminals.containsKey(point)
                    && ((myParentCircuitEnginePanel.myDraggedComponent.headTerminal == myParentCircuitEnginePanel.myDraggedTerminal
                    && (myParentCircuitEnginePanel.myDraggedComponent.tailTerminal == myParentCircuitEnginePanel.myNearestTerminal
                    || myParentCircuitEnginePanel.myDraggedComponent.tailTerminal.isConnectedTo(myParentCircuitEnginePanel.myNearestTerminal)))
                    || (myParentCircuitEnginePanel.myDraggedComponent.tailTerminal == myParentCircuitEnginePanel.myDraggedTerminal
                    && (myParentCircuitEnginePanel.myDraggedComponent.headTerminal == myParentCircuitEnginePanel.myNearestTerminal
                    || myParentCircuitEnginePanel.myDraggedComponent.headTerminal.isConnectedTo(myParentCircuitEnginePanel.myNearestTerminal))))) //!(myParentCircuitEnginePanel.myGridPoints.containsKey(point)&&
                    //  (myParentCircuitEnginePanel.myDraggedTerminal
                    // .isConnectedTo(myParentCircuitEnginePanel.myGridPoints.get(point))||
                    // myParentCircuitEnginePanel.myDraggedTerminal
                    //.isConnectedToOrder2(myParentCircuitEnginePanel.myGridPoints.get(point))))
                    ) {
                //System.out.println("r2");
                //System.out.prntln();
                myParentCircuitEnginePanel.myCircuit.myTerminals.remove(oldPoint);
            }
            if (!myParentCircuitEnginePanel.myCircuit.myTerminals.containsKey(point)) {//that is if there is no point at this location yet
                myParentCircuitEnginePanel.myCircuit.myTerminals.put(point, newTerminal);//then put a point there
                if (myParentCircuitEnginePanel.myDraggedComponent.headTerminal
                        == myParentCircuitEnginePanel.myDraggedTerminal) {//and set the location of the head
                    myParentCircuitEnginePanel.myDraggedComponent
                            .setHead(myParentCircuitEnginePanel.myCircuit.myTerminals.get(point));
                    myParentCircuitEnginePanel.myDraggedTerminal = myParentCircuitEnginePanel.myDraggedComponent.headTerminal;
                } else {//or tail of the dragged component accordingly
                    myParentCircuitEnginePanel.myDraggedComponent
                            .setTail(myParentCircuitEnginePanel.myCircuit.myTerminals.get(point));
                    //myParentCircuitEnginePanel.myDraggedComponent.setTail(newTerminal);
                    myParentCircuitEnginePanel.myDraggedTerminal = myParentCircuitEnginePanel.myDraggedComponent.tailTerminal;
                }
            } else {//if there is a point here already
                //myParentCircuitEnginePanel.myGridPoints.put(point, newTerminal);
                // System.out.println("4");
                if ((myParentCircuitEnginePanel.myDraggedComponent.headTerminal == myParentCircuitEnginePanel.myDraggedTerminal
                        && (myParentCircuitEnginePanel.myDraggedComponent.tailTerminal == myParentCircuitEnginePanel.myNearestTerminal
                        || myParentCircuitEnginePanel.myDraggedComponent.tailTerminal.isConnectedTo(myParentCircuitEnginePanel.myNearestTerminal)))//if this is the head and is connectedorder2 to nearesttiepoint 
                        || (myParentCircuitEnginePanel.myDraggedComponent.tailTerminal == myParentCircuitEnginePanel.myDraggedTerminal
                        && (myParentCircuitEnginePanel.myDraggedComponent.headTerminal == myParentCircuitEnginePanel.myNearestTerminal
                        || myParentCircuitEnginePanel.myDraggedComponent.headTerminal.isConnectedTo(myParentCircuitEnginePanel.myNearestTerminal))) //myParentCircuitEnginePanel.myDraggedTerminal
                        //.isConnectedTo(myParentCircuitEnginePanel.myGridPoints.get(point))||
                        //myParentCircuitEnginePanel.myDraggedTerminal
                        //.isConnectedToOrder2(myParentCircuitEnginePanel.myGridPoints.get(point))
                        // myParentCircuitEnginePanel.myNearestTerminal.isConnectedTo(
                        // myParentCircuitEnginePanel.myDraggedTerminal)||
                        //myParentCircuitEnginePanel.myNearestTerminal.isConnectedToOrder2(
                        //myParentCircuitEnginePanel.myDraggedTerminal)
                        ) {
                    //System.out.println("connected");
                    //System.out.println(myParentCircuitEnginePanel.myNearestTerminal.getY());
                    //System.out.println(myParentCircuitEnginePanel.myDraggedTerminal.getY());
                    return;//then don't do anything, we can't move here
                }
                //if we reach this point, it means that we are moving the component to a location that already has a point
                if (myParentCircuitEnginePanel.myDraggedComponent.headTerminal
                        == myParentCircuitEnginePanel.myDraggedTerminal) {//if we're moving the head of draggedcomp
                    myParentCircuitEnginePanel.myDraggedComponent
                            .setHead(myParentCircuitEnginePanel.myCircuit.myTerminals.get(point));//then move the head
                    myParentCircuitEnginePanel.myDraggedTerminal = myParentCircuitEnginePanel.myDraggedComponent.headTerminal;
                } else {
                    myParentCircuitEnginePanel.myDraggedComponent
                            .setTail(myParentCircuitEnginePanel.myCircuit.myTerminals.get(point));
                    //myParentCircuitEnginePanel.myDraggedComponent.setTail(newTerminal);
                    myParentCircuitEnginePanel.myDraggedTerminal = myParentCircuitEnginePanel.myDraggedComponent.tailTerminal;
                }
            }
            myParentCircuitEnginePanel.myNearestTerminal = myParentCircuitEnginePanel.myDraggedTerminal;
            myParentCircuitEnginePanel.repaint();
        }

    }
}
