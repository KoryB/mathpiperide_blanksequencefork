package org.mathpiper.ui.gui.applications.circuitpiper.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.util.LinkedList;
import java.util.Stack;
import javax.swing.JCheckBox;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.Timer;

import org.mathpiper.ui.gui.applications.circuitpiper.model.Circuit;
import org.mathpiper.ui.gui.applications.circuitpiper.model.Terminal;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.ACCurrentSource;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.ACVoltageSource;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.Ammeter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.Block;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.CapacitanceMeter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.Capacitor;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.CurrentIntegrator;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.CurrentSource;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.Component;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.CCCS;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.CCVS;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.transistors.bipolar.TransistorNPN;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.InductanceMeter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.Inductor;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.Ohmmeter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.Resistor;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.Switch;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.VCCS;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.VCVS;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.VoltageIntegrator;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.VoltageSource;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.transistors.bipolar.TransistorPNP;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.transistors.fet.TransistorJFETN;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.active.transistors.fet.TransistorJFETP;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.integratedcircuits.LogicalPackage;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.meters.Voltmeter;
import org.mathpiper.ui.gui.applications.circuitpiper.model.components.passive.Wire;

/*AUTHORS:

 - Kevin Stueve (2009-12-20): initial published version
 #*****************************************************************************
 #       Copyright (C) 2009 Kevin Stueve kstueve@uw.edu
 #
 #  Distributed under the terms of the GNU General Public License (GPL)
 #                  http://www.gnu.org/licenses/
 #*****************************************************************************
 */
public final class CircuitEnginePanel extends JPanel {

    boolean isProbe = false;
    boolean myIsMovingPoint;
    boolean myIsMovingComponent;
    boolean myIsDrawing;
    int myMouseX, myMouseY;
    boolean myButtonState;//true if pressed
    boolean myMouseIsEntered;//true if mouse is in DrawingPanel
    public final int leftSideOffsetPixels = 4;
    public final int topSideYOffsetPixels = 4;
    public final double terminalXSpacing = 31.5;
    public final double terminalYSpacing = 31.5;
    public final int widthPixels = 1200;
    public final int heightPixels = 800;
    public int xDistanceBetweenTerminalsPixels = (int) ((widthPixels - leftSideOffsetPixels) / terminalXSpacing);
    public int yDistanceBetweenTerminalsPixels = (int) ((heightPixels - topSideYOffsetPixels) / terminalYSpacing);
    HintPanel myHintPanel;
    public Component myTempComponent; //Holds a newly created component.
    public Component myNearestComponent;
    public Component myDraggedComponent;
    public Switch myNearestSwitch;
    public Terminal myNearestTerminal;
    public Terminal myDraggedTerminal;
    public String mySelectedComponent;
    public Circuit myCircuit;
    public LinkedList<PhasePlane> phasePlanes;

    public final DrawingPanel drawingPanel;
    public boolean isBusy;
    public boolean waiting;
    public boolean isRunning;
    public String defaultCapacitorErrorString = "1.0E-9";
    public String defaultInductorErrorString = "1.0E-9";
    public String capacitorErrorString = defaultCapacitorErrorString;
    public String inductorErrorString = defaultInductorErrorString;
    public double defaultCapacitorError = Math.pow(10, -9);
    public double defaultInductanceError = Math.pow(10, -9);//was 10^-11
    public double capacitorError = defaultCapacitorError;
    public double inductanceError = defaultInductanceError;
    public boolean timeOption = true;
    public JPanel screenCapturePanel;
    public Timer myTimer;
    public JCheckBox showGridCheckBox;

    public CircuitEnginePanel() throws Exception {
        super();

        this.setBackground(Color.WHITE);
        myCircuit = new Circuit(this);
        phasePlanes = new LinkedList<PhasePlane>();
        this.setLayout(new BorderLayout());
        //this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        myHintPanel = new HintPanel(this);
        //JPanel bp = new ButtonPanel(this);
        //bp.setLayout(new BorderLayout());
        this.add(new ButtonPanel(this), BorderLayout.NORTH);
        //JPanel outerdp=new JPanel();
        //outerdp.setLayout(new BorderLayout());
        //this.add(bp);
        drawingPanel = new DrawingPanel(this);


        screenCapturePanel = new JPanel();
        screenCapturePanel.setBackground(Color.WHITE);

        screenCapturePanel.add(drawingPanel);

        JPanel circuitBox = new JPanel(new BorderLayout());

        PanelController treePanelScaler = new PanelController(drawingPanel, drawingPanel.viewScale);

        JScrollPane panelScrollPane = new JScrollPane(screenCapturePanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        panelScrollPane.getVerticalScrollBar().setUnitIncrement(16);
        panelScrollPane.getViewport().setBackground(Color.WHITE);
        
        circuitBox.add(panelScrollPane);

        JPanel horizontalPanel = new JPanel();
        horizontalPanel.add(treePanelScaler);

        circuitBox.add(horizontalPanel, BorderLayout.SOUTH);
        
        //todo:tk:simulation speed appears to be controlled here
        // and in Circuit.java timeStep = .05.
        // see CirSim.runCircuit().
        myTimer = new Timer(50, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (isBusy || !isRunning) {
                    return;
                }

                // System.out.println("a");
                synchronized (drawingPanel) {//##changed from synchronized this on Dec 29 2008
                    try {
                        CircuitEnginePanel.this.myCircuit.updateCircuit();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    drawingPanel.repaint();
                }
                //System.out.println("b");
            }
        });

        myTimer.setRepeats(true);
        myTimer.start();
        
        this.add(circuitBox, BorderLayout.CENTER);
        
        myHintPanel.startingHint();
        //JPanel southPanel= new JPanel();
        //outerdp.add(dp,BorderLayout.CENTER);
        //this.add(outerdp);
        //southPanel.add(myHintPanel,BorderLayout.SOUTH);
        //this.add(myHintPanel);
        this.add(myHintPanel, BorderLayout.SOUTH);
        //this.add(southPanel,BorderLayout.SOUTH);
        this.revalidate();
        setIsMovingPoint(false);
        setIsMovingComponent(false);
        setIsDrawing(false);
    }

    public void setIsMovingPoint(final boolean theIsMovingPoint) {
        this.myIsMovingPoint = theIsMovingPoint;
    }

    public boolean isMovingPoint() {
        return myIsMovingPoint;
    }

    public boolean isDrawing() {
        return myIsDrawing;
    }

    public void setIsDrawing(final boolean theIsDrawing) {
        this.myIsDrawing = theIsDrawing;
    }

    public boolean isMovingComponent() {
        return myIsMovingComponent;
    }

    public void setIsMovingComponent(final boolean theIsMovingComponent) {
        this.myIsMovingComponent = theIsMovingComponent;
    }

    public void setMouseX(final int theMouseX) {
        myMouseX = (int) Math.round(theMouseX / drawingPanel.viewScale);
    }

    public int getMouseX() {
        return myMouseX;
    }

    public void setMouseY(final int theMouseY) {
        myMouseY = (int) Math.round(theMouseY / drawingPanel.viewScale);
    }

    public int getMouseY() {
        return myMouseY;
    }

    public void setButtonState(final boolean theButtonState) {
        myButtonState = theButtonState;
    }

    public void setMouseEntered(final boolean theMouseEntered) {
        myMouseIsEntered = theMouseEntered;
    }

    public boolean getMouseEntered() {
        return myMouseIsEntered;
    }

    public void setHintDrawing() {
        if (!myHintPanel.hint.equals("drawing")) {
            myHintPanel.drawingHint();
        }
    }

    public void setHintStarting() {
        if (!myHintPanel.hint.equals("starting")) {
            myHintPanel.startingHint();
        }
    }

    public void hintNearTerminal() {
        if (!myHintPanel.hint.equals("terminal")) {
            myHintPanel.nearTerminalHint();
        }
    }

    public void hintNearComponent() {
        if (!myHintPanel.hint.equals("component")) {
            myHintPanel.nearComponentHint();
        }
    }

    public void hintNearSwitch() {
        if (!myHintPanel.hint.equals("switch")) {
            myHintPanel.nearSwitchHint();
        }
    }

    public Component newComponent(String typeAndUID, int xPixels, int yPixels, Stack attributes) throws Exception {
        Component newComponent = null;
        
        String componentType = "";
        String uid = null;
        
        if(typeAndUID.contains("_"))
        {
            String[] typeAndUIDArray = typeAndUID.split("_");
            componentType = typeAndUIDArray[0];
            uid = typeAndUIDArray[1];
        }
        else
        {
            componentType = typeAndUID;
        }

        if (componentType.startsWith("Ammeter")) {
            newComponent = new Ammeter(xPixels, yPixels, uid, drawingPanel);
        } else if (componentType.startsWith("VoltageSource")) {
            if(attributes == null)
            {
                newComponent = new VoltageSource(xPixels, yPixels, uid, drawingPanel);
            }
            else
            {
                newComponent = new VoltageSource(xPixels, yPixels, uid, attributes, drawingPanel);
            }
        } else if (componentType.startsWith("CapacitanceMeter")) {
            newComponent = new CapacitanceMeter(xPixels, yPixels, uid, drawingPanel);
        } else if (componentType.startsWith("Capacitor")) {
            if (attributes == null) {
                newComponent = new Capacitor(xPixels, yPixels, uid, drawingPanel);
            } else {
                newComponent = new Capacitor(xPixels, yPixels, uid, attributes, drawingPanel);
            }
        } else if (componentType.startsWith("CurrentSource")) {
            if (attributes == null) {
                newComponent = new CurrentSource(xPixels, yPixels, uid, drawingPanel);
            } else {
                newComponent = new CurrentSource(xPixels, yPixels, uid, attributes, drawingPanel);
            }
        } else if (componentType.startsWith("CurrentIntegrator")) {
            newComponent = new CurrentIntegrator(xPixels, yPixels, uid, drawingPanel);
        } else if (componentType.startsWith("VoltageIntegrator")) {
            newComponent = new VoltageIntegrator(xPixels, yPixels, uid, drawingPanel);
        } else if (componentType.startsWith("InductanceMeter")) {
            newComponent = new InductanceMeter(xPixels, yPixels, uid, drawingPanel);
        } else if (componentType.startsWith("Inductor")) {
            if (attributes == null) {
                newComponent = new Inductor(xPixels, yPixels, uid, drawingPanel);
            } else {
                newComponent = new Inductor(xPixels, yPixels, uid, attributes, drawingPanel);
            }
        } else if (componentType.startsWith("Ohmmeter")) {
            newComponent = new Ohmmeter(xPixels, yPixels, uid, drawingPanel);
        } else if (componentType.startsWith("Resistor")) {
            if (attributes == null) {
                newComponent = new Resistor(xPixels, yPixels, uid, drawingPanel);
            } else {
                newComponent = new Resistor(xPixels, yPixels, uid, attributes, drawingPanel);
            }
        } else if (componentType.startsWith("Switch")) {
            newComponent = new Switch(xPixels, yPixels, uid, drawingPanel);
        } else if (componentType.startsWith("Voltmeter")) {
            newComponent = new Voltmeter(xPixels, yPixels, uid, drawingPanel);
        } else if (componentType.startsWith("Wire")) {
            newComponent = new Wire(xPixels, yPixels, uid, drawingPanel);
        } else if (componentType.startsWith("ACCurrentSource")) {
            newComponent = new ACCurrentSource(xPixels, yPixels, uid, drawingPanel);
        } else if (componentType.startsWith("ACVoltageSource")) {
            newComponent = new ACVoltageSource(xPixels, yPixels, uid, drawingPanel);
        } else if (componentType.startsWith("Block")) {
            if (attributes == null) {
                newComponent = new Block(xPixels, yPixels, uid, drawingPanel);
            } else {
                newComponent = new Block(xPixels, yPixels, uid, attributes, drawingPanel);
            }
        } else if (componentType.startsWith("VCVS")) {
            newComponent = new VCVS(xPixels, yPixels, uid, drawingPanel);
        } else if (componentType.startsWith("VCCS")) {
            newComponent = new VCCS(xPixels, yPixels, uid, drawingPanel);
        } else if (componentType.startsWith("CCVS")) {
            newComponent = new CCVS(xPixels, yPixels, uid, drawingPanel);
        }else if (componentType.startsWith("CCCS")) {
            newComponent = new CCCS(xPixels, yPixels, uid, drawingPanel);
        } else if (componentType.startsWith("TransistorNPN")) {
            newComponent = new TransistorNPN(xPixels, yPixels, uid, drawingPanel);
        } else if (componentType.startsWith("TransistorPNP")) {
            newComponent = new TransistorPNP(xPixels, yPixels, uid, drawingPanel);
        } else if (componentType.startsWith("TransistorJFETN")) {
            newComponent = new TransistorJFETN(xPixels, yPixels, uid, drawingPanel);
        } else if (componentType.startsWith("TransistorJFETP")) {
            newComponent = new TransistorJFETP(xPixels, yPixels, uid, drawingPanel);
        } else if (componentType.startsWith("LogicalPackage")) {
            newComponent = new LogicalPackage(xPixels, yPixels, uid, drawingPanel);
        } else {
            throw new Exception("Unknown component name: " + componentType);
        }
        

        return newComponent;

    }

    public void newTempComponent(String name, int xPixels, int yPixels) throws Exception {

        myTempComponent = newComponent(name, xPixels, yPixels, null);

        //String componentKey = myTempComponent.toString();
        //Class cl = Class.forName("Model.Components."+mySelectedComponent.replaceAll(" ", ""));
        //Constructor cons = cl.getConstructor(Integer.TYPE,Integer.TYPE);
        //Point p = new Point(nearestX(),nearestY());
        //myTempComponent=(Component)cons.newInstance(nearestX(),nearestY());
        //myTempComponent=(Component)cons.newInstance(nearestX(),nearestY());
    /*} catch (InvocationTargetException e) { 
         //System.out.println("InvocationTargetException");
         } catch (NoSuchMethodException e) {  
         //System.out.println("NoSuchMethodException");
         } catch (ClassNotFoundException e) {
         //System.out.println("ClassNotFoundException");
         } catch (IllegalAccessException e) {
         //System.out.println("IllegalAccessException");
         } catch (InstantiationException e) {
         //System.out.println("InstantiationException");
         }*/
    }

    public void setSelectedComponent(final String theSelectedButton) {
        mySelectedComponent = theSelectedButton;
    }

    public int nearestTerminalXPixels() {
        long column = Math.round((getMouseX() - leftSideOffsetPixels) / terminalXSpacing);
        column = Math.max(column, 0);
        column = Math.min(column, xDistanceBetweenTerminalsPixels - 1);
        int xPixels = (int) Math.round(leftSideOffsetPixels + column * terminalXSpacing);
        return xPixels;
    }

    public int nearestTerminalYPixels() {
        long row = Math.round((getMouseY() - topSideYOffsetPixels) / terminalYSpacing);
        row = Math.max(row, 0);
        row = Math.min(row, yDistanceBetweenTerminalsPixels - 1);
        int yPixels = (int) Math.round(topSideYOffsetPixels + row * terminalYSpacing);
        return yPixels;
    }

    public void addElectricComponent(Component component, int tailXPixels, int tailYPixels) {
        Point headPoint = new Point(component.headTerminal.getX(), component.headTerminal.getY());
        if (!myCircuit.myTerminals.containsKey(headPoint)) {
            myCircuit.myTerminals.put(headPoint, component.headTerminal);
        }
        component.setHead(myCircuit.myTerminals.get(headPoint));

        Point tailPoint = new Point(tailXPixels, tailYPixels);
        if (!myCircuit.myTerminals.containsKey(tailPoint)) {
            myCircuit.myTerminals.put(tailPoint, component.tailTerminal);
        }
        component.setTail(myCircuit.myTerminals.get(tailPoint));
        //component.connectHeadAndTail();
        myCircuit.electricComponents.add(component);
    }

    public boolean nearTerminal() {
        boolean result = false;
        if (myCircuit.myTerminals.containsKey(new Point(nearestTerminalXPixels(), nearestTerminalYPixels()))
                && sqr(getMouseX() - nearestTerminalXPixels()) + sqr(getMouseY() - nearestTerminalYPixels()) < 10 * 10) {
            result = true;
            myNearestTerminal = myCircuit.myTerminals.get(new Point(nearestTerminalXPixels(), nearestTerminalYPixels()));
        }
        return result;
    }

    public double sqr(double x) {
        return x * x;
    }

    public boolean nearComponent() {
        int x3 = getMouseX();
        int y3 = getMouseY();
        double mindistance = Double.MAX_VALUE;
        boolean result = false;
        for (Component electricComponent : myCircuit.electricComponents) {
            int x1 = electricComponent.headTerminal.getX();
            int y1 = electricComponent.headTerminal.getY();
            int x2 = electricComponent.tailTerminal.getX();
            int y2 = electricComponent.tailTerminal.getY();
            double u = ((x3 - x1) * (x2 - x1) + (y3 - y1) * (y2 - y1)) / (sqr(x2 - x1) + sqr(y2 - y1));
            double x = x1 + u * (x2 - x1);
            double y = y1 + u * (y2 - y1);
            if (0 < u && u < 1 && sqr(x3 - x) + sqr(y3 - y) < 300) {
                if (sqr(x3 - x) + sqr(y3 - y) < mindistance) {
                    mindistance = sqr(x3 - x) + sqr(y3 - y);
                    myNearestComponent = electricComponent;
                    double d1 = sqr(electricComponent.headTerminal.getX() - x3) + sqr(electricComponent.headTerminal.getY() - y3);
                    double d2 = sqr(electricComponent.tailTerminal.getX() - x3) + sqr(electricComponent.tailTerminal.getY() - y3);
                    if (d1 < d2) {
                        myNearestTerminal = electricComponent.headTerminal;
                    } else {
                        myNearestTerminal = electricComponent.tailTerminal;
                    }
                }
                result = true;
            }
        }
        return result;
    }

    public boolean nearSwitch() {
        int x3 = getMouseX();
        int y3 = getMouseY();
        double mindistance = Double.MAX_VALUE;
        boolean result = false;
        for (Component electricComponent : myCircuit.electricComponents) {
            if (electricComponent.getClass() != Switch.class) {
                continue;
            }
            int x1 = electricComponent.headTerminal.getX();
            int y1 = electricComponent.headTerminal.getY();
            int x2 = electricComponent.tailTerminal.getX();
            int y2 = electricComponent.tailTerminal.getY();
            double u = ((x3 - x1) * (x2 - x1) + (y3 - y1) * (y2 - y1)) / (sqr(x2 - x1) + sqr(y2 - y1));
            double x = x1 + u * (x2 - x1);
            double y = y1 + u * (y2 - y1);
            if (0 < u && u < 1 && sqr(x3 - 0.5 * (x1 + x2)) + sqr(y3 - 0.5 * (y1 + y2)) < 300) {
                if (sqr(x3 - x) + sqr(y3 - y) < mindistance) {
                    mindistance = sqr(x3 - x) + sqr(y3 - y);
                    myNearestSwitch = (Switch) electricComponent;
                }
                result = true;
            }
        }
        return result;
    }


    
    public void print()
    {
        PrinterJob printJob = PrinterJob.getPrinterJob();
        boolean isNotCancel = printJob.printDialog();

        if(isNotCancel)
        {
            Paper paper = new Paper();

            //paper.setSize(2000, 2000);

            PageFormat pageFormat = new PageFormat();

            pageFormat.setOrientation(PageFormat.LANDSCAPE);
            pageFormat.setPaper(paper);

            printJob.setPrintable(drawingPanel, pageFormat);
            try {
                printJob.print();
            } catch (PrinterException p) {
                p.printStackTrace();
            }
        }
    }
    
    public Circuit getCircuit()
    {
        return myCircuit;
    }
    
}
