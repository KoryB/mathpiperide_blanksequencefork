
package org.mathpiper.ui.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class TreeFeaturesPanel extends JPanel{
    
    private List<ActionListener> actionListeners;
    
    private JTextField patternTextField = null;
    
    private Map<String, Component> componentsMap = new HashMap<String, Component>();
    
    
    public TreeFeaturesPanel()
    {
        setLayout(new BorderLayout());
        
        actionListeners = new ArrayList<ActionListener>();
        
        Box box = new Box(BoxLayout.Y_AXIS);
        
        
        JPanel buttonsPanel = new JPanel();
        
        JPanel zoomButtonsPanel = new JPanel();
       
        buttonsPanel.add(zoomButtonsPanel);

        JButton decrease = new JButton("Zoom-");
        decrease.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                Font font = patternTextField.getFont();
                font = font.deriveFont((float) (font.getSize2D() * .90));
                patternTextField.setFont(font);
                TreeFeaturesPanel.this.revalidate();
            }
        });
        zoomButtonsPanel.add(decrease);

        JButton increase = new JButton("Zoom+");
        increase.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                Font font = patternTextField.getFont();
                font = font.deriveFont((float) (font.getSize2D() * 1.10));
                patternTextField.setFont(font);
                TreeFeaturesPanel.this.revalidate();
            }
        });
        zoomButtonsPanel.add(increase);
        

        add(buttonsPanel, BorderLayout.NORTH);

        
        JPanel centerPanel = new JPanel();
        
        centerPanel.setLayout(new FlowLayout(FlowLayout.LEADING,0, 0));

        JLabel messageLabel = new JLabel("Note: select two nodes to measure the distance between them.");

        box.add(messageLabel);
        
        box.add(new JLabel("Enter a pattern."));
        
        patternTextField = new JTextField(15);
        
        componentsMap.put("patternTextField", patternTextField);
        
        Font font1 = new Font("Mono", Font.PLAIN, 20);
        
        patternTextField.setFont(font1);
        
        patternTextField.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                String patternString = patternTextField.getText();
                
                
                
                ActionEvent ae = new ActionEvent(this, ActionEvent.ACTION_PERFORMED, patternString);
                
                notifyListeners(ae);
            }

        });
        
        box.add(patternTextField);
        
        centerPanel.add(box);
        
        this.add(centerPanel);
    }
    
    
    public void addActionListener(ActionListener listener) {
        actionListeners.add(listener);
    }

    public void removeActionListener(ActionListener listener) {
        actionListeners.remove(listener);
    }

    protected void notifyListeners(ActionEvent ae) {

        for (ActionListener listener : actionListeners) {
            listener.actionPerformed(ae);

        }//end for.

    }//end method.
    
    
    public List getComponentLocation(String name) throws Throwable
    {
        Component component = this.componentsMap.get(name);
        
        if(component == null)
        {
            throw new Exception("The name <" + name + "> does not exist.");
        }
        
        Point onScreenLocation = component.getLocationOnScreen();
        
        List coordinatesList = new ArrayList();
        
        coordinatesList.add(onScreenLocation.getX());
        coordinatesList.add(onScreenLocation.getY());
        
        return coordinatesList;
    }
}
