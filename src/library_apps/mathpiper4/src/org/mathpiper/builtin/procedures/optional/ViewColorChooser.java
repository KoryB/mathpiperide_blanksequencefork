/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:

package org.mathpiper.builtin.procedures.optional;

import java.awt.Color;
import javax.swing.JColorChooser;
import org.mathpiper.builtin.BuiltinProcedure;
import static org.mathpiper.builtin.BuiltinProcedure.setTopOfStack;
import org.mathpiper.builtin.BuiltinProcedureEvaluator;
import org.mathpiper.builtin.JavaObject;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.cons.BuiltinObjectCons;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.lisp.cons.NumberCons;
import org.mathpiper.lisp.cons.SublistCons;

/**
 *
 *
 */
public class ViewColorChooser extends BuiltinProcedure
{

    public void plugIn(Environment aEnvironment) throws Throwable
    {
        this.functionName = "ViewColorChooser";
        aEnvironment.getBuiltinFunctions().put(this.functionName, new BuiltinProcedureEvaluator(this, 0, BuiltinProcedureEvaluator.FixedNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));
    }//end method.

    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        Cons listCons = AtomCons.getInstance(aEnvironment.getPrecision(), "List");
        
        Color color = JColorChooser.showDialog(null, "Choose a Color", null);
        
        int red = color.getRed();
        Cons redCons = new NumberCons(((Integer)red).toString(), 10);
   
        listCons.setCdr(redCons);
        
        int green = color.getGreen();
        Cons greenCons = new NumberCons(((Integer)green).toString(), 10);
        
        redCons.setCdr(greenCons);
        
        int blue = color.getBlue();
        Cons blueCons = new NumberCons(((Integer)blue).toString(), 10);
        
        greenCons.setCdr(blueCons);
        
        Cons sublistCons = SublistCons.getInstance(listCons);
        
        if(color != null)
        {
            //JavaObject response = new JavaObject(color);
            //setTopOfStack(aEnvironment, aStackTop, BuiltinObjectCons.getInstance(aEnvironment, aStackTop, response));
            
            setTopOfStack(aEnvironment, aStackTop, sublistCons);
        }
        else
        {
            setTopOfStack(aEnvironment, aStackTop, Utility.getFalseAtom(aEnvironment));
        }

    }//end method.

}//end class.




/*
%mathpiper_docs,name="ViewColorChooser",categories="Programming Procedures;Built In",access="experimental"
*CMD ViewConsole --- show a color chooser dialog
*CORE
*CALL
    ViewColorChooser()

*DESC

Show a color chooser dialog.

*E.G.

In> ViewColorChooser()
Result: [200,52,123]

%/mathpiper_docs
*/