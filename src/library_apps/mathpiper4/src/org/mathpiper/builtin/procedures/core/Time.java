/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:

package org.mathpiper.builtin.procedures.core;

import java.math.BigDecimal;
import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.lisp.unparsers.MathPiperUnparser;

/**
 *
 *  
 */
public class Time extends BuiltinProcedure
{


    private Time()
    {
    }

    public Time(Environment aEnvironment, String functionName)
    {
        this.functionName = functionName;

        aEnvironment.iBodiedProcedures.setOperator(MathPiperUnparser.KMaxPrecedence, "Time");
    }//end constructor.

    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        BigDecimal startTime = new BigDecimal(System.nanoTime());

        Cons result = aEnvironment.iLispExpressionEvaluator.evaluate(aEnvironment, aStackTop, getArgument(aEnvironment, aStackTop, 1));

        BigDecimal endTime = new BigDecimal(System.nanoTime());

        BigDecimal timeDiff;

        timeDiff = endTime.subtract(startTime);

        timeDiff = timeDiff.movePointLeft(9);
        
        setTopOfStack(aEnvironment, aStackTop, AtomCons.getInstance(aEnvironment.getPrecision(), "" + timeDiff));
    }
}



/*
%mathpiper_docs,name="Time",categories="Programming Procedures;Input/Output;Built In"
*CMD Time --- measure the time taken by an evaluation
*CORE
*CALL
	Time() expr
*PARMS
{expr} -- any expression
*DESC
The procedure {Time() expr} evaluates the expression {expr} and returns the time needed for the evaluation.
The result is returned as a floating-point number of seconds.
The value of the expression {expr} is lost.

The result is the "user time" as reported by the OS, not the real ("wall clock") time.
Therefore, any CPU-intensive processes running alongside MathPiper will not significantly affect the result of {Time}.

*E.G.
In> Time() Simplify((a*b)/(b*a))
Result: 0.09;

*SEE EchoTime, SystemTimer

%/mathpiper_docs
*/