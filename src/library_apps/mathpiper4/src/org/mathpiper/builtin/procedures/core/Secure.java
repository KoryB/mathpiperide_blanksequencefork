/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:

package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.lisp.Environment;


/**
 *
 *  
 */
public class Secure extends BuiltinProcedure
{

    private Secure()
    {
    }

    public Secure(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        boolean prevSecure = aEnvironment.iSecure;
        aEnvironment.iSecure = true;
        try
        {
            setTopOfStack(aEnvironment, aStackTop, aEnvironment.iLispExpressionEvaluator.evaluate(aEnvironment, aStackTop, getArgument(aEnvironment, aStackTop, 1)));
        } catch (Throwable e)
        {
            throw e;
        } finally
        {
            aEnvironment.iSecure = prevSecure;
        }
    }
}



/*
%mathpiper_docs,name="Secure",categories="Programming Procedures;Built In"
*CMD Secure --- guard the host OS
*CORE
*CALL
	Secure(body)

*PARMS

{body} -- expression

*DESC

{Secure} evaluates {body} in a "safe" environment, where files cannot be opened
and system calls are not allowed. This can help protect the system
when e.g. a script is sent over the Internet to be evaluated on a remote computer,
which is potentially unsafe.

%/mathpiper_docs

*SEE SystemCall
*/