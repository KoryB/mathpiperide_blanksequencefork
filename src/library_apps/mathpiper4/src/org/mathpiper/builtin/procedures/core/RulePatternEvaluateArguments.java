/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:

package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.lisp.Environment;

/**
 *
 *  
 */
public class RulePatternEvaluateArguments extends BuiltinProcedure
{

    private RulePatternEvaluateArguments()
    {
    }

    public RulePatternEvaluateArguments(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        org.mathpiper.lisp.Utility.newRule(aEnvironment, aStackTop, true);
    }
}




/*
%mathpiper_docs,name="RulePatternEvaluateArguments",categories="Programming Procedures;Miscellaneous;Built In"
*CMD RulePatternEvaluateArguments --- defines a rule which uses a pattern as its predicate

*CALL
	RulePatternEvaluateArguments("operator", arity, precedence, pattern) body
*PARMS

{"operator"} -- string, name of function

{arity}, {precedence} -- integers

{pattern} -- a pattern object

{body} -- expression, body of rule

*DESC
This function defines a rule which uses a pattern as its predicate.

*SEE RulePatternHoldArguments
%/mathpiper_docs
*/