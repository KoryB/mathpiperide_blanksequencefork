/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.BuiltinProcedure;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;
import static org.mathpiper.builtin.BuiltinProcedure.setTopOfStack;
import org.mathpiper.lisp.cons.AtomCons;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;

/**
 *
 *
 */
public class EditDistance extends BuiltinProcedure
{

    private EditDistance()
    {
    }

    public EditDistance(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        if( getArgument(aEnvironment, aStackTop, 1) == null) LispError.checkArgument(aEnvironment, aStackTop, 1);
        String s1 = (String) getArgument(aEnvironment, aStackTop, 1).car();
        if( s1 == null) LispError.checkArgument(aEnvironment, aStackTop, 1);
        if(s1.charAt(0) != '\"') LispError.checkArgument(aEnvironment, aStackTop, 1);
        if(s1.charAt(s1.length() - 1) != '\"') LispError.checkArgument(aEnvironment, aStackTop, 1);
                
        if( getArgument(aEnvironment, aStackTop, 2) == null) LispError.checkArgument(aEnvironment, aStackTop, 2);
        String s2 = (String) getArgument(aEnvironment, aStackTop, 2).car();
        if( s2 == null) LispError.checkArgument(aEnvironment, aStackTop, 2);
        if(s2.charAt(0) != '\"') LispError.checkArgument(aEnvironment, aStackTop, 2);
        if(s2.charAt(s2.length() - 1) != '\"') LispError.checkArgument(aEnvironment, aStackTop, 2);

        
        //Levenshtein Edit Distance Algorithm.
        //Obtained from https://gist.github.com/gabhi/11243437
        int edits[][] = new int[s1.length() + 1][s2.length() + 1];
        for (int i = 0; i <= s1.length(); i++)
        {
            edits[i][0] = i;
        }
        for (int j = 1; j <= s2.length(); j++)
        {
            edits[0][j] = j;
        }
        for (int i = 1; i <= s1.length(); i++)
        {
            for (int j = 1; j <= s2.length(); j++)
            {
                int u = (s1.charAt(i - 1) == s2.charAt(j - 1) ? 0 : 1);
                edits[i][j] = Math.min(
                        edits[i - 1][j] + 1,
                        Math.min(
                                edits[i][j - 1] + 1,
                                edits[i - 1][j - 1] + u
                        )
                );
            }
        }

        setTopOfStack(aEnvironment, aStackTop, AtomCons.getInstance(aEnvironment.getPrecision(), "" + edits[s1.length()][s2.length()]));
    }
}



/*
%mathpiper_docs,name="EditDistance",categories="Programming Procedures;Strings;Built In",access="experimental"
*CMD EditDistance --- calculates the edit distance between two strings
*CORE
*CALL
	EditDistance(s1, s2)

*PARMS
 {s1} - a string
 {s2} - a string

*DESC
This procedure calculates the edit distance between two strings.

*E.G.
In> EditDistance("Hello", "hello")
Result: 1

%/mathpiper_docs
*/
