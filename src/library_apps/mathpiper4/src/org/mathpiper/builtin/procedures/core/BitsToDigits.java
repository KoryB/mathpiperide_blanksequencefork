/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:

package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.BigNumber;
import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.exceptions.EvaluationException;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.Utility;

/**
 *
 *  
 */
public class BitsToDigits extends BuiltinProcedure
{

    private BitsToDigits()
    {
    }

    public BitsToDigits(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        BigNumber x = org.mathpiper.lisp.Utility.getNumber(aEnvironment, aStackTop, 1);
        BigNumber y = org.mathpiper.lisp.Utility.getNumber(aEnvironment, aStackTop, 2);
        long result = 0;  // initialize just in case

        if (x.isInteger() && x.isSmall() && y.isInteger() && y.isSmall())
        {
            // bits_to_digits uses unsigned long, see numbers.h
            int base = (int) y.toDouble();
            result = Utility.bitsToDigits((long) (x.toDouble()), base);
        } else
        {
            throw new EvaluationException("BitsToDigits: error: arguments (" + x.toDouble() + ", " + y.toDouble() + ") must be small integers",aEnvironment.getCurrentInput().iStatus.getSourceName(), aEnvironment.getCurrentInput().iStatus.getLineNumber(), -1, aEnvironment.getCurrentInput().iStatus.getLineIndex());
        }
        BigNumber z = new BigNumber(aEnvironment.getPrecision());
        z.setTo((long) result);
        setTopOfStack(aEnvironment, aStackTop, new org.mathpiper.lisp.cons.NumberCons(z));
    }
}

/*
%mathpiper_docs,name="BitsToDigits",categories="Programming Procedures;Native Objects;Built In"
*CMD BitsToDigits --- changes 2 small ints to digits
*CORE
*CALL
	BitsToDigits(int1,int2)

*DESC
Takes two small ints and changes them into digits to be expressed in binary.

%/mathpiper_docs
*/
