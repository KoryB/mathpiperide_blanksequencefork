/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.optional;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.mathpiper.builtin.BuiltinProcedure;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;
import static org.mathpiper.builtin.BuiltinProcedure.setTopOfStack;
import org.mathpiper.builtin.BuiltinProcedureEvaluator;
import org.mathpiper.builtin.JavaObject;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;

import org.mathpiper.lisp.cons.BuiltinObjectCons;
import org.mathpiper.lisp.cons.Cons;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;

public class DemoGUI extends BuiltinProcedure
{
    private static JFrame demoGUIFrame = null;

    private Map defaultOptions;

    public void plugIn(Environment aEnvironment) throws Throwable
    {
        this.functionName = "DemoGUI";

        aEnvironment.getBuiltinFunctions().put("DemoGUI", new BuiltinProcedureEvaluator(this, 0, BuiltinProcedureEvaluator.VariableNumberOfArguments | BuiltinProcedureEvaluator.EvaluateArguments));

        defaultOptions = new HashMap();
        defaultOptions.put("Title", null);
        defaultOptions.put("returnContent", false);
        defaultOptions.put("Width", null);
        defaultOptions.put("Height", null);
        defaultOptions.put("Center", false);

    }//end method.

    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        
        /*if(demoGUIFrame != null)
        {
            JavaObject response = new JavaObject(demoGUIFrame);

            setTopOfStack(aEnvironment, aStackTop, BuiltinObjectCons.getInstance(aEnvironment, aStackTop, response));
        }
        */

        Cons arguments = getArgument(aEnvironment, aStackTop, 1);

        if (!Utility.isSublist(arguments))
        {
            LispError.throwError(aEnvironment, aStackTop, LispError.INVALID_ARGUMENT, "ToDo");
        }

        arguments = (Cons) arguments.car(); //Go to sub list.

        arguments = arguments.cdr(); //Strip List tag.

        Cons options = arguments;

        Map userOptions = Utility.optionsListToJavaMap(aEnvironment, aStackTop, options, defaultOptions);

        final Box box = Box.createVerticalBox();

        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        Container contentPane = frame.getContentPane();
        frame.setBackground(Color.WHITE);
        contentPane.setBackground(Color.WHITE);

        frame.setAlwaysOnTop(true);
        frame.setTitle((String) userOptions.get("title"));

        JPanel jPanel = new JPanel()
        {

            @Override // placeholder for actual content
            public Dimension getPreferredSize()
            {
                return new Dimension(200, 200);
            }

        };
        
        
        final Environment fEnvironment = aEnvironment;
        final int fStackTop = aStackTop;

        JButton jButton;
                
        jButton = new JButton("Continue");
        jButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ae)
            {
                try
                {

                    fEnvironment.setLocalOrGlobalVariable(fStackTop, "continue", Utility.getTrueAtom(fEnvironment), false, false, false);
                } catch (Throwable t)
                {
                    t.printStackTrace();
                }
            }
        });
        jPanel.add(jButton);
        
        
       jButton = new JButton("Stop");
        jButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ae)
            {
                try
                {

                    fEnvironment.setLocalOrGlobalVariable(fStackTop, "stop", Utility.getTrueAtom(fEnvironment), false, false, false);
                } catch (Throwable t)
                {
                    t.printStackTrace();
                }
            }
        });
        jPanel.add(jButton);
        
        
        

        contentPane.add(jPanel);

        frame.setResizable(true);
        frame.pack();

        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice defaultScreen = ge.getDefaultScreenDevice();
        Rectangle rect = defaultScreen.getDefaultConfiguration().getBounds();
        int x = (int) rect.getMaxX() - frame.getWidth();
        int y = (int) rect.getMaxY() - frame.getHeight();
        frame.setLocation(x, y);
        frame.setVisible(true);

        if ((Boolean) userOptions.get("Center"))
        {
            frame.setLocationRelativeTo(null);
        }

        JScrollPane scrollPane = new JScrollPane(box, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPane.getVerticalScrollBar().setUnitIncrement(16);

        contentPane.add(scrollPane);

        JMenu fileMenu = new JMenu("File");

        JMenuItem saveAsImageAction = new JMenuItem();
        saveAsImageAction.setText("Save As Image");
        saveAsImageAction.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ae)
            {
                org.mathpiper.ui.gui.Utility.saveImageOfComponent(box);
            }
        });
        fileMenu.add(saveAsImageAction);

        JMenuBar menuBar = new JMenuBar();

        menuBar.add(fileMenu);

        frame.setJMenuBar(menuBar);

        frame.setVisible(true);

        JavaObject response = new JavaObject(frame);

        setTopOfStack(aEnvironment, aStackTop, BuiltinObjectCons.getInstance(aEnvironment, aStackTop, response));

    }//end method.

}//end class.

/*
 %mathpiper_docs,name="DemoGUI",categories="Programming Procedures;Visualization",access="experimental"
 *CMD DemoGUI --- a GUI to control automated demonstrations of programs
 *CORE
 *CALL
 DemoGui(option, option, option...)

 *PARMS

 {Options:}

 {Title} -- the title of the window


 *DESC

 This GUI is used to control automated demonstrations of programs.



 *E.G.
 In> DemoGui()


 %/mathpiper_docs
 */
