/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.core;


import org.mathpiper.builtin.BuiltinProcedure;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;
import static org.mathpiper.builtin.BuiltinProcedure.setTopOfStack;
import org.mathpiper.exceptions.BreakException;
import org.mathpiper.exceptions.ContinueException;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;

import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.Cons;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;


/**
 *
 *  
 */
public class Until extends BuiltinProcedure
{

    private Until()
    {
    }

    public Until(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable {
        Cons arg1 = getArgument(aEnvironment, aStackTop, 1);
        Cons arg2 = getArgument(aEnvironment, aStackTop, 2);

        //Cons predicate = aEnvironment.iLispExpressionEvaluator.evaluate(aEnvironment, aStackTop, arg1);
        Cons predicate;

        Cons evaluated = Utility.getFalseAtom(aEnvironment);
        
        int beforeStackTop = -1;
        int beforeEvaluationDepth = -1;
               
        try {
            do {
                beforeStackTop = aEnvironment.iArgumentStack.getStackTopIndex();
                beforeEvaluationDepth = aEnvironment.iEvalDepth;

                try {

                    evaluated = aEnvironment.iLispExpressionEvaluator.evaluate(aEnvironment, aStackTop, arg2);

                } catch (ContinueException ce) {
                    aEnvironment.iArgumentStack.popTo(beforeStackTop, aStackTop, aEnvironment);
                    aEnvironment.iEvalDepth = beforeEvaluationDepth;
                    setTopOfStack(aEnvironment, aStackTop, Utility.getTrueAtom(aEnvironment));
                }//end continue catch.

                predicate = aEnvironment.iLispExpressionEvaluator.evaluate(aEnvironment, aStackTop, arg1);

            } while (Utility.isFalse(aEnvironment, predicate, aStackTop));

        } catch (BreakException be) {
              aEnvironment.iArgumentStack.popTo(beforeStackTop, aStackTop, aEnvironment);
              aEnvironment.iEvalDepth = beforeEvaluationDepth;
        }

        setTopOfStack(aEnvironment, aStackTop, evaluated);
    }

}

/*
%mathpiper_docs,name="Until",categories="Programming Procedures;Control Flow"
*CMD Until --- loop until a condition is met
*STD
*CALL
        Until(pred) body

*PARMS

{pred} -- predicate deciding whether to stop

{body} -- expression to loop over

*DESC

Keep on evaluating "body" until "pred" becomes {True}. More precisely, {Until} first
evaluates the expression "body". Then the predicate "pred" is
evaluated, which should yield either {True} or {False}. In the latter case, the expressions "body"
and "pred" are again evaluated and this continues as long as
"pred" is {False}. As soon as "pred" yields {True}, the loop terminates and {Until} returns {True}.

The main difference with {While} is that {Until} always evaluates the body at 
least once, but {While} may not evaluate the body at all. Besides, the
meaning of the predicate is reversed: {While} stops
if "pred" is {False} while {Until} stops if "pred" is {True}.
The command
{Until(pred) body;} is equivalent to {pred; While(!? pred) body;}. In fact, the
implementation of {Until} is based on the internal
command {While}. The {Until}
command can be compared to the {do ... while}
construct in the programming language C.

*E.G.

/%mathpiper
x := 1;

Until(x >=? 5)
{
   Echo(x); 
   x++;
};
/%/mathpiper

    /%output,sequence="4",timestamp="2013-09-16 22:36:36.156",preserve="false"
      Result: True
      
      Side Effects:
      1 
      2 
      3 
      4 
      
.   /%/output

*SEE While, For, ForEach, Break, Continue
%/mathpiper_docs
 */
