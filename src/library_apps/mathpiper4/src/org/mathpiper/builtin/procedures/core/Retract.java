/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.BuiltinProcedure;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;

import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.Cons;

/**
 *
 *  
 */
public class Retract extends BuiltinProcedure
{

    private Retract()
    {
    }

    public Retract(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable
    {
        // Get operator
        Cons evaluated = getArgument(aEnvironment, aStackTop, 1);

        if( evaluated == null) LispError.checkArgument(aEnvironment, aStackTop, 1);
        String orig = (String) evaluated.car();

        orig = Utility.stripEndQuotesIfPresent(orig);
        
        if( orig == null) LispError.checkArgument(aEnvironment, aStackTop, 1);
        String oper = Utility.getSymbolName(aEnvironment, orig);

        Cons arityCons = getArgument(aEnvironment, aStackTop, 2);
        if(!(arityCons.car() instanceof String)) LispError.checkArgument(aEnvironment, aStackTop, 2);
        String arityString = (String) arityCons.car();
        if(arityString.equalsIgnoreCase("All"))
        {
            aEnvironment.retractRule(oper, -1, aStackTop, aEnvironment);
        }
        else
        {
            int arity = Integer.parseInt(arityString, 10);
            aEnvironment.retractRule(oper, arity, aStackTop, aEnvironment);
        }
  
        setTopOfStack(aEnvironment, aStackTop, Utility.getTrueAtom(aEnvironment));
    }
}



/*
%mathpiper_docs,name="Retract",categories="Programming Procedures;Built In"
*CMD Retract --- delete rules for a procedure
*CORE
*CALL
	Retract("procedure",arity)

*PARMS
{"procedure"} -- string, name of procedure

{arity} -- positive integer or All

*DESC

Remove a rulebase for the procedure named {"procedure"} with the specific {arity}, if it exists. This will make
MathPiper forget all rules defined for a given procedure with the given arity. Rules for procedures with
the same name but different arities are not affected unless the All constant is used.  If All is used for the
arity, then all arities of the rulebase are removed.

Assignment {:=} of a procedure automatically does a single arity retract to the procedure being (re)defined.

*SEE RulebaseArgumentsList, RulebaseHoldArguments, :=
%/mathpiper_docs
*/