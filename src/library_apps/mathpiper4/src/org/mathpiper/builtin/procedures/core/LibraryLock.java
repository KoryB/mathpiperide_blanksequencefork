/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
package org.mathpiper.builtin.procedures.core;

import org.mathpiper.builtin.BuiltinProcedure;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;
import static org.mathpiper.builtin.BuiltinProcedure.setTopOfStack;
import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.Utility;
import org.mathpiper.lisp.cons.Cons;
import static org.mathpiper.builtin.BuiltinProcedure.getArgument;

/*
 * Unlocks and locks the library so library functions can be redefined or protected from redefinition.
 *  
 */
public class LibraryLock extends BuiltinProcedure
{

    private LibraryLock()
    {
    }

    public LibraryLock(String functionName)
    {
        this.functionName = functionName;
    }


    public void evaluate(Environment aEnvironment, int aStackTop) throws Throwable {
        
        
        Cons argument = getArgument(aEnvironment, aStackTop, 1);
        
        if(Utility.isBoolean(aEnvironment, argument, aStackTop) != true) LispError.throwError(aEnvironment, aStackTop, "The argument must evalute to True or False.");

        if (Utility.isTrue(aEnvironment, argument, aStackTop) )
        {
            Environment.lockLibrary = true;
            aEnvironment.write("The library is locked.\n");       
        } else
        {
            Environment.lockLibrary = false;
            aEnvironment.write("The library is unlocked.\n");
        }
        
        setTopOfStack(aEnvironment, aStackTop, Utility.getTrueAtom(aEnvironment));
        
    }//end method.
    
}//end class.



/*
%mathpiper_docs,name="LibraryLock",categories="Programming Procedures;Built In",access="private"
*CMD LibraryLock --- unlock and lock the script library
*CALL
	LibraryLock(predicate)

*PARMS
{predicate} - True or False

*DESC
By default the script library is locked and it cannot be changed. Unlocking the library
enables the changing and retracting of library functions and rules. Locking the library prevents
the changing and retracting of library functions and rules.

*E.G.
In> Echo()
Result: True
Side Effects:



In> Echo() := 3
Result: Exception
Exception: The procedure <Echo> is a library procedure, and library procedures cannot be redefined.  Starting at index 7.

In> LibraryLock(False)
Result: True
Side Effects:
The library is unlocked.


In> Echo() := 3
Result: True

In> Echo()
Result: 3

In> LibraryLock(True)
Result: True
Side Effects:
The library is locked.


In> Echo() := 3
Result: Exception
Exception: The procedure <Echo> is a library procedure, and library procedures cannot be redefined.  Starting at index 7.

*SEE PipeToFile, PipeFromString, ParseMathPiper, ParseMathPiperToken
%/mathpiper_docs





%mathpiper,name="LibraryLock",subtype="automatic_test"
{
    // Just test that trying to redefine or retract a library script works for now.
    Local(v1);
    LibraryLock(True);
    Verify(ExceptionCatch(Echo(_a) := 4 , "", "Exception"), "Exception");
    Verify(ExceptionCatch(Retract("Echo", All) , "", "Exception"), "Exception");
};
%/mathpiper
*/