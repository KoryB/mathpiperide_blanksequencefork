%mathpiper,def="DotProduct;DotProduct0;DotProduct1"

LocalSymbols(DotProduct0,DotProduct1)
{

// vector . vector
DotProduct(t1_Vector?,t2_Vector?)::(Length(t1) =? Length(t2)) <--
   DotProduct0(t1,t2,Length(t1));

// matrix . vector
DotProduct(t1_Matrix?,t2_Vector?)::(Length(t1[1]) =? Length(t2)) <--
{
   Local(i,n,m,result);
   n := Length(t1);
   m := Length(t2);
   result := List();
   For(i := 1,i <=? n,i++)
        Insert!(result,1,DotProduct0(t1[i],t2,m));
   Reverse!(result);
}

// vector . matrix
DotProduct(t1_Vector?,t2_Matrix?)::(Length(t1) =? Length(t2)
                               &? Length(t2[1]) >? 0) <--
   DotProduct1(t1,t2,Length(t1),Length(t2[1]));

// matrix . matrix
DotProduct(t1_Matrix?,t2_Matrix?)::(Length(t1[1]) =? Length(t2)
                                  &? Length(t2[1]) >? 0) <--
{
   Local(i,n,k,l,result);
   n := Length(t1);
   k := Length(t2);
   l := Length(t2[1]);
   result := List();
   For(i := 1,i <=? n,i++)
      Insert!(result,1,DotProduct1(t1[i],t2,k,l));
   Reverse!(result);
}

// vector . vector
DotProduct0(t1_,t2_,n_) <--
{
   Local(i,result);
   result := 0;
   For(i := 1,i <=? n,i++)
      result := result + t1[i]*t2[i];
   result;
}

// vector . matrix
// m vector length
// n number of matrix cols
DotProduct1(t1_,t2_,m_,n_) <--
{
   Local(i,j,result);
   result := ZeroVector(n);
   For(i := 1,i <=? n,i++)
      For(j := 1,j <=? m,j++)
         result[i] := result[i] + t1[j]*t2[j][i];
   result;
}

} // LocalSymbols(DotProduct0,DotProduct1)

%/mathpiper



%mathpiper_docs,name="DotProduct",categories="Mathematics Procedures;Linear Algebra"
*CMD DotProduct --- get dot product of tensors
*STD
*CALL
        DotProduct(t1,t2)

*PARMS

{t1,t2} -- tensor lists (currently only vectors and matrices are supported)

*DESC

{DotProduct} returns the dot (aka inner) product of two tensors t1 and t2. The last
index of t1 and the first index of t2 are contracted. Currently {DotProduct} works
only for vectors and matrices. {DotProduct}-multiplication of two vectors, a matrix
with a vector (and vice versa) or two matrices yields either a scalar, a
vector or a matrix.

*E.G.

In> DotProduct([1,2],[3,4])
Result: 11;

In> DotProduct([[1,2],[3,4]],[5,6])
Result: [17,39];

In> DotProduct([5,6],[[1,2],[3,4]])
Result: [23,34];

In> DotProduct([[1,2],[3,4]],[[5,6],[7,8]])
Result: [[19,22],[43,50]];

*SEE OuterProduct, CrossProduct, Scalar?, Vector?, Matrix?
%/mathpiper_docs





%mathpiper,name="DotProduct",subtype="automatic_test"

// vector . vector
Verify(DotProduct([],[]),0);
Verify(DotProduct([],_a),Hold(DotProduct([],_a)));
Verify(DotProduct(_a,[]),Hold(DotProduct(_a,[])));
Verify(DotProduct([_a],[]),Hold(DotProduct([_a],[])));
Verify(DotProduct([],[_a]),Hold(DotProduct([],[_a])));
Verify(DotProduct([_a],[_b]),_a*_b);
Verify(DotProduct([_a],[_b,_c]),Hold(DotProduct([_a],[_b,_c])));
Verify(DotProduct([_a,_b],[_c]),Hold(DotProduct([_a,_b],[_c])));
Verify(DotProduct([_a,_b],[_c,_d]),_a*_c+_b*_d);
Verify(DotProduct([_a,_b],[_c,[_d]]),Hold(DotProduct([_a,_b],[_c,[_d]])));
Verify(DotProduct([_a,[_b]],[_c,_d]),Hold(DotProduct([_a,[_b]],[_c,_d])));
Verify(DotProduct([_a,_b],[_c,_d,_e]),Hold(DotProduct([_a,_b],[_c,_d,_e])));
Verify(DotProduct([_a,_b,_c],[_d,_e]),Hold(DotProduct([_a,_b,_c],[_d,_e])));
Verify(DotProduct([1,2,3],[4,5,6]),32);

// matrix . vector
Verify(DotProduct([[]],[]),[0]);
Verify(DotProduct([[]],[1]),Hold(DotProduct([[]],[1])));
Verify(DotProduct([[],[]],[]),[0,0]);
Verify(DotProduct([[_a]],[_b]),[_a*_b]);
Verify(DotProduct([[_a],[_b]],[_c]),[_a*_c,_b*_c]);
Verify(DotProduct([[1],[2]],[2]),[2,4]);
Verify(DotProduct([[1,2,3],[4,5,6]],[7,8,9]),[50,122]);

// vector . matrix
Verify(DotProduct([],[[]]),Hold(DotProduct([],[[]])));
Verify(DotProduct([],[[],[]]),Hold(DotProduct([],[[],[]])));
Verify(DotProduct([1],[[]]),Hold(DotProduct([1],[[]])));
Verify(DotProduct([1],[[],[]]),Hold(DotProduct([1],[[],[]])));
Verify(DotProduct([_a,_b],[[_c],[_d]]),[_a*_c+_b*_d]);
Verify(DotProduct([1,2,3],[[4,5],[6,7],[8,9]]),[40,46]);

// matrix . matrix
Verify(DotProduct([[]],[[]]),Hold(DotProduct([[]],[[]])));
Verify(DotProduct([[_a]],[[]]),Hold(DotProduct([[_a]],[[]])));
Verify(DotProduct([[]],[[_b]]),Hold(DotProduct([[]],[[_b]])));
Verify(DotProduct([[1,2],[3,4],[5,6]],[[1,2,3],[4,5,6]]),[[9,12,15],[19,26,33],[29,40,51]]);
Verify(DotProduct([[1,2,3],[4,5,6]],[[1,2],[3,4],[5,6]]),[[22,28],[49,64]]);

%/mathpiper