%mathpiper,def="Segment"

Segment(PointA_Point?,PointB_Point?) <--
{
    Local(x1,x2,y1,y2);
    
    x1 := PointA[1];
    x2 := PointB[1];
    y1 := PointA[2];
    y2 := PointB[2];

    [[x1,y1],[x2,y2]];
}

%/mathpiper



%mathpiper_docs,name="Segment",categories="Mathematics Procedures;Analytic Geometry",access="experimental"
*CMD Segment --- returns a list which contains the endpoints of a segment
*STD
*CALL
        Segment(p1, p2)
*PARMS

{p1} -- the first endpoint

{p2} -- the second endpoint

*DESC

This procedure returns a list which represents a segment by its endpoints.

*E.G.


In> PointA := Point(2,3)
Result: [2,3]
        

In> PointB := Point(6,8)
Result: [6,8]
        

In> Segment(PointA,PointB)
Result: [[2,3],[6,8]]

*SEE Point?, Point, Distance, Slope
%/mathpiper_docs
