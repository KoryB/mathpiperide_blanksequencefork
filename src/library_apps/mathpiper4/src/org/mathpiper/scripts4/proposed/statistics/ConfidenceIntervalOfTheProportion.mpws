%mathpiper,def="ConfidenceIntervalOfTheProportion"

ConfidenceIntervalOfTheProportion(numberOfSuccesses,sampleSize,confidenceLevel) :=
{
    Check(Integer?(numberOfSuccesses) &? numberOfSuccesses >=? 0, "The first argument must be an integer which is >=?0");
    
    Check(Integer?(sampleSize) &? sampleSize >=? 0, "The second argument must be an integer which is >=?0");
    
    Local(criticalZScore,approximateStandardErrorOfTheProportion,upperLimit,lowerLimit,resultList,proportion);
    
    resultList := [];
    
    criticalZScore := ConfidenceLevelToZScore(confidenceLevel);
    
    resultList["criticalZScore"] := criticalZScore;
    
    proportion := NM(numberOfSuccesses/sampleSize);
    
    approximateStandardErrorOfTheProportion := Sqrt((proportion*(1 - proportion))/sampleSize);
    
    upperLimit := NM(proportion + criticalZScore * approximateStandardErrorOfTheProportion);
    
    lowerLimit := NM(proportion - criticalZScore * approximateStandardErrorOfTheProportion);
    
    Decide(Verbose?(),
        {
            Echo("Critical z-score: ", criticalZScore);
            
            Echo("Proportion: ", proportion);
            
            Echo("Standard error of the proportion: ", NM(approximateStandardErrorOfTheProportion));
        });
    
    resultList["upperLimit"] := upperLimit;
    
    resultList["lowerLimit"] := lowerLimit;
    
    resultList;
}


%/mathpiper

    %output,preserve="false"
      Result: True
.   %/output





%mathpiper_docs,name="ConfidenceIntervalOfTheProportion",categories="Mathematics Procedures;Statistics & Probability",access="experimental"
*CMD ConfidenceIntervalOfTheProportion --- calculates a confidence interval for a proportion
*STD
*CALL
        ConfidenceIntervalOfTheProportion(numberOfSuccesses,sampleSize,confidenceLevel)
*PARMS
{numberOfSuccesses} -- the number of successes in the sample
{sampleSize} -- the size of the sample
{confidenceLevel} -- the desired confidence level

*DESC

This procedure calculates a confidence interval for a proportion.  It returns an association list
which contains the lower limit, the upper limit, and the critical Z score.

*E.G.

In> ConfidenceIntervalOfTheProportion(110,175,.90)
Result: [["lowerLimit",0.5684923463],["upperLimit",0.6886505109],["criticalZScore",1.644853952]]

*SEE ConfidenceIntervalOfTheMean, Verbose, Association
%/mathpiper_docs