%mathpiper,def="OptionsToAssociationList"
OptionsToAssociationList(optionList) :=
{
    Local(associativeList, key, value);
    
    associativeList := [];
    
    ForEach(option, optionList)
    {
        Decide(option[0] =? ToAtom(":"),
        {
            Decide(String?(option[1]), key := option[1], key := ToString(option[1]));
            Decide(String?(option[2]) |? Number?(option[2]) |? Constant?(option[2]) |? List?(option[2]) |? Procedure?(option[2]) |? Atom?(option[2]), value := option[2], value := ToString(option[2]));
            
            associativeList := Concat([[key, value]], associativeList);
        
        });
    
    }
    associativeList;
}

%/mathpiper



%mathpiper_docs,name="OptionsToAssociationList",categories="Programming Procedures;Lists (Operations)",access="experimental"
*CMD OptionsToAssociationList --- converts an options list into an associative list
*CALL
        OptionsToAssociationList(optionsList)

*PARMS
{optionsList} -- an options list to be converted into an associative list

*DESC
This procedure converts a list of options in the form of {name: value, name: value}
into an associative list.

*E.G.
In> OptionsToAssociationList([a: 1, b: 2])
Result> [["b",2],["a",1]]

%/mathpiper_docs




%mathpiper,name="OptionsToAssociationList",subtype="automatic_test"

Verify( OptionsToAssociationList([ lines: True, labels: False ]), [["labels",False],["lines",True]]);

%/mathpiper



