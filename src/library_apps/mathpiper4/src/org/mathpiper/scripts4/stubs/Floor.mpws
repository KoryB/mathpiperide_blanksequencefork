%mathpiper,def="Floor"

5 # Floor(Infinity) <-- Infinity;
5 # Floor(-Infinity) <-- -Infinity;
5 # Floor(Undefined) <-- Undefined;


10 # Floor(x_RationalOrNumber?)
   <--
   {
     x:=NM(Eval(x));
//Echo("x = ",x);
     Local(prec,result,n);
     Assign(prec,BuiltinPrecisionGet());
     Decide(Zero?(x),
       Assign(n,2),
       Decide(x>?0,
         Assign(n,2+FloorN(NM(FastLog(x)/FastLog(10)))),
         Assign(n,2+FloorN(NM(FastLog(-x)/FastLog(10))))
       ));
     Decide(n>?prec,BuiltinPrecisionSet(n));
//Echo("Before");
     Assign(result,FloorN(x));
//Echo("After");
     BuiltinPrecisionSet(prec);
     result;
   }

//     FloorN(NM(x));


//todo:tk:should this be removed because it is no longer needed?
/* Changed by Nobbi before redefinition of Rational
10 # Floor(x_Number?) <-- FloorN(x);
10 # Ceil (x_Number?) <-- CeilN (x);
10 # Round(x_Number?) <-- FloorN(x+0.5);

20 # Floor(x_Rational?):: (Number?(Numerator(x)) &? Number?(Denominator(x))) <-- FloorN(NM(x));
20 # Ceil (x_Rational?):: (Number?(Numerator(x)) &? Number?(Denominator(x))) <-- CeilN (NM(x));
20 # Round(x_Rational?):: (Number?(Numerator(x)) &? Number?(Denominator(x))) <-- FloorN(NM(x+0.5));
*/

%/mathpiper



%mathpiper_docs,name="Floor",categories="Mathematics Procedures;Numbers (Operations)"
*CMD Floor --- round a number downwards
*STD
*CALL
        Floor(x)

*PARMS

{x} -- a number

*DESC

This procedure returns $Floor(x)$, the largest integer smaller than or equal to $x$.

*E.G.

In> Floor(1.1)
Result: 1;

In> Floor(-1.1)
Result: -2;

*SEE Ceil, Round
%/mathpiper_docs





%mathpiper,name="Floor",subtype="automatic_test"

// Discovered that Floor didn't handle new exponent notation
Verify(Floor(1001.1e-1),100);
Verify(Floor(10.01e1),100);
Verify(Floor(100.1),100);

%/mathpiper