%mathpiper,def="LogicVerify"

Procedure("LogicVerify",["aLeft", "aRight"])
{
  Decide(aLeft !=? aRight,
    Verify(CanProve(aLeft  ->?  aRight),True)
  );
}

%/mathpiper



%mathpiper_docs,name="LogicVerify",categories="Programming Procedures;Testing"
*CMD LogicVerify --- verifying equivalence of two expressions
*STD
*CALL
        LogicVerify(question,answer)

*PARMS

{question} -- expression to check for

{answer} -- expected result after evaluation


*DESC

The command {LogicVerify} can be used to verify that an
expression is <I>equivalent</I> to  a correct answer after evaluation.
It returns {True} or {False}

*E.G.
In> LogicVerify(a &? c |? b &? !? c,a |? b)
Result: True;

In> LogicVerify(a &? c |? b &? !? c,b |? a)
Result: True;

*SEE KnownFailure, Verify, TestMathPiper, LogicTest

%/mathpiper_docs

*SEE Simplify, CanProve