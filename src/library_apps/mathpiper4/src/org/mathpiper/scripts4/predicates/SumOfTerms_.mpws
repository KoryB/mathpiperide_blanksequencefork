%mathpiper,def="SumOfTerms?"

// an expression free of the variable -- obviously not a sum of terms in it
10 # SumOfTerms?(var_,expr_FreeOf?(var)) <-- False;

// an Atom cannot be a sum of terms
12 # SumOfTerms?(var_,expr_Atom?()) <-- False;

// after being "Listified", expr is a sum of terms if headed by "+" or "-"
14 # SumOfTerms?(var_,expr_List?())::(expr[1]=?ToAtom("+") |? expr[1]=?ToAtom("-")) <-- True;

// after being "Listified", an expr headed by "*" is not considered a sum 
// of terms unless one or the other operand is free of the variable
16 # SumOfTerms?(var_,expr_List?())::(expr[1]=?ToAtom("*")) <-- Or?(FreeOf?(var,expr[2]),FreeOf?(var,expr[3]));

// after being "Listified", an expr headed by "/" is not considered a sum 
// of terms unless the denominator (only) is free of the variable
18 # SumOfTerms?(var_,expr_List?())::(expr[1]=?ToAtom("/")) <-- FreeOf?(var,expr[3]);

// after being "Listified", any other expression is not a sum of terms
20 # SumOfTerms?(var_,expr_List?()) <-- False;

// if we get to this point, ProcedureToList the expression and try again
22 # SumOfTerms?(var_,expr_) <-- SumOfTerms?(var,ProcedureToList(expr));

%/mathpiper

%mathpiper_docs,name="SumOfTerms?",categories="Programming Procedures;Predicates"
*CMD SumOfTerms? --- check for expression being a sum of terms in variable

*STD
*CALL
        SumOfTerms?(var,expr)

*PARMS

{var} -- a variable name

{expr} -- an expression to be tested

*DESC

The command {SumOfTerms?} returns {True} if the expression {expr} can be 
considered to be a "sum of terms" in the given variable {var}.  The criteria
are reasonable but somewhat arbitrary.  The criteria were selected after
a lot of experimentation, specifically to aid the logic used in Integration.

The criteria for {expr} to be a sum of terms in {var} are:
     o  {expr} is a function of variable {var}
     o  {expr} can best be described as a sum (or difference) of two or more
        procedures of {var}  OR
        {expr} is a monomial in {var} (this latter is to simplify the logic)
     o  {expr} is not better described as a product of procedures of {var}
     o  {expr} is not better described as a quotient of procedures of {var}
        (i.e., is a rational function)

Note that the last three criteria are somewhat subjective!  

*E.G.
In> SumOfTerms?(x,23)
Result> False

In> SumOfTerms?(x,23*x)
Result> True

In> SumOfTerms?(x,5*y)
Result> False

In> SumOfTerms?(x,a*x^2-b*x-c/x)
Result> True

In> SumOfTerms?(x,Sin(x))
Result> False

In> SumOfTerms?(x,Sin(x)+Exp(x))
Result> True

In> SumOfTerms?(x,d*(x^2-1))
Result> True

In> SumOfTerms?(x,(x^2-1)*d)
Result> True

In> SumOfTerms?(x,(x^2-1)/d)
Result> True

In> SumOfTerms?(x,d/(x^2-1))
Result> False

In> SumOfTerms?(x,(x^2-1)*(x^2+1))
Result> False

In> SumOfTerms?(x,(x^2-1)/(x^2+1))
    Result> False    
    
%/mathpiper_docs
