%mathpiper,def="NegativeNumber?"

NegativeNumber?(x):= Number?(x) &? x <? 0;

%/mathpiper



%mathpiper_docs,name="NegativeNumber?",categories="Programming Procedures;Predicates"
*CMD NegativeNumber? --- test for a negative number
*STD
*CALL
        NegativeNumber?(n)

*PARMS

{n} -- number to test

*DESC

{NegativeNumber?(n)} evaluates to {True} if $n$ is (strictly) negative, i.e.
if $n<0$. If {n} is not a number, the procedures return {False}. 

*E.G.

In> NegativeNumber?(6);
Result: False;

In> NegativeNumber?(-2.5);
Result: True;

*SEE Number?, PositiveNumber?, NotZero?, NegativeInteger?, NegativeReal?
%/mathpiper_docs