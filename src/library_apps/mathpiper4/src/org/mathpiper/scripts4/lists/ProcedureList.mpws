%mathpiper,def="ProcedureList;ProcedureListAll;ProcedureListHelper"

Retract("ProcedureListHelper", All);
Retract("ProcedureList", All);
Retract("ProcedureListAll", All);

//////////////////////////////////////////////////
/// ProcedureList --- list all procedure atoms used in an expression
//////////////////////////////////////////////////
/// like VarList except collects procedures

10 # ProcedureListHelper(expr_Atom?) <-- [];
20 # ProcedureListHelper(expr_Procedure?) <--
Concat(
    [ToString(First(ProcedureToList(expr)))],
    Apply("Concat",
            MapSingle("ProcedureListHelper", Rest(ProcedureToList(expr)))
    )
);


10 # ProcedureListAll(expr_Atom?) <-- [];
20 # ProcedureListAll(expr_Procedure?) <-- `(ProcedureListHelper(@expr));


10 # ProcedureList(expr_Atom?) <-- [];
20 # ProcedureList(expr_Procedure?) <-- RemoveDuplicates(`(ProcedureListHelper(@expr)));

/*
This is like ProcedureList except only looks at arguments of a given list of procedures. All other procedures become "opaque".

*/
10 # ProcedureList(expr_Atom?, looklist_List?) <-- [];

// a procedure not in the looking list - return its type
20 # ProcedureList(expr_Procedure?, looklist_List?)::(!? Contains?(looklist, ToString(Type(expr)))) <-- [ToAtom(Type(expr))];

// a procedure in the looking list - traverse its arguments
30 # ProcedureList(expr_Procedure?, looklist_List?) <--
RemoveDuplicates(
        Concat(
                [First(ProcedureToList(expr))],
                {        // gave up trying to do it using Map and MapSingle... so writing a loop now.
                        // obtain a list of procedures, considering only procedures in looklist
                        Local(item, result);
                        result := [];
                        ForEach(item, expr) result := Concat(result, ProcedureList(item, looklist));
                        result;
                }
        )
);

HoldArgumentNumber("ProcedureListHelper", 1, 1);


%/mathpiper

    %output,sequence="9",timestamp="2015-10-03 23:03:42.164",preserve="false"
      Result: True
.   %/output




%mathpiper_docs,name="ProcedureList;ProcedureListAll",categories="Programming Procedures;Lists (Operations)"
*CMD ProcedureList --- list of procedures used in an expression
*CMD ProcedureListAll --- list of procedures used in an expression
*CMD ProcedureListArithmetic --- list of procedures used in an expression
*CMD ProcedureListSome --- list of procedures used in an expression
*STD
*CALL
        ProcedureList(expr)
        ProcedureListAll(expr)
        ProcedureListArithmetic(expr)
        ProcedureListSome(expr, list)

*PARMS

{expr} -- an expression

{list} -- list of procedure atoms to be considered "transparent"

*DESC

The command {ProcedureList(expr)} returns a list of all procedure atoms that appear
in the expression {expr} with no duplicates. The expression is recursively 
traversed.

The command {ProcedureListAll(expr)} is similar to ProcedureList, except it does not
remove duplicates.

The command {ProcedureListSome(expr, list)} does the same, except it only looks 
at arguments of a given {list} of procedures. All other procedures become 
"opaque" (as if they do not contain any other procedures).
For example, {ProcedureListSome(a + Sin(b-c))} will see that the expression has 
a "{-}" operation and return {[+,Sin,-]}, but {ProcedureListSome(a + Sin(b-c), [+])} 
will not look at arguments of {Sin()} and will return {[+,Sin]}.

{ProcedureListArithmetic} is defined through {ProcedureListSome} to look only at arithmetic operations {+}, {-}, {*}, {/}.

Note that since the operators "{+}" and "{-}" are prefix as well as infix operators, 
it is currently required to use {ToAtom("+")} to obtain the unevaluated atom "{+}".

*E.G. notest

In> ProcedureList(x+y*Cos(Ln(x)/x))
Result: [+,*,Cos,/,Ln];

In> ProcedureListArithmetic(x+y*Cos(Ln(x)/x))
Result: [+,*,Cos];

In> ProcedureListSome([a+b*2,c/d],[List])
Result: [List,+,/];

*SEE VarList, HasExpression?, HasProcedure?
%/mathpiper_docs