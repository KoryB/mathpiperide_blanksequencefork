%mathpiper,def="Swap"

Procedure("Swap",["list", "index1", "index2"])
{
  Local(item1,item2);
  item1:=list[index1];
  item2:=list[index2];
  list[index1] := item2;
  list[index2] := item1;
}

%/mathpiper



%mathpiper_docs,name="Swap",categories="Programming Procedures;Lists (Operations)"
*CMD Swap --- swap two elements in a list
*STD
*CALL
        Swap(list, i1, i2)

*PARMS

{list} -- the list in which a pair of entries should be swapped

{i1, i2} -- indices of the entries in "list" to swap

*DESC

This command swaps the pair of entries with entries "i1" and "i2"
in "list". So the element at index "i1" ends up at index "i2"
and the entry at "i2" is put at index "i1". Both indices should be
valid to address elements in the list. Then the updated list is
returned.

{Swap()} works also on generic arrays.

*E.G.

In> lst := [_a,_b,_c,_d,_e,_f];
Result: [_a,_b,_c,_d,_e,_f];

In> Swap(lst, 2, 4);
Result: [_a,_d,_c,_b,_e,_f];

*SEE Replace, Replace!, ArrayCreate
%/mathpiper_docs