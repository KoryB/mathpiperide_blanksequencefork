%mathpiper,def="VarList"

VarList(expression) :=
{
    RemoveDuplicates(VarListAll(expression));
}

%/mathpiper



%mathpiper_docs,name="VarList",categories="Programming Procedures;Lists (Operations)"
*CMD VarList --- list of variables appearing in an expression (without duplicates)
*STD
*CALL
        VarList(expr)

*PARMS

{expr} -- an expression

{list} -- a list of procedure atoms

*DESC

The command {VarList(expr)} returns a list of all variables that appear in 
the expression {expr}. The expression is traversed recursively.

Note that since the operators "{+}" and "{-}" are prefix as well as infix 
operators, it is currently required to use {ToAtom("+")} to obtain the 
unevaluated atom "{+}".

*E.G.

In> VarList(Sin(_x))
Result: [_x];

In> VarList(_x+_a*_y)
Result: [_x,_a,_y];

In> VarList(2+_a*3)
Result: [_a]

*SEE VarListArith, VarListSome, VarListAll, FreeOf?, Variable?, ProcedureList, HasExpression?, HasProcedure?, UnderscoreConstants, UnderscoreConstantsAll
%/mathpiper_docs