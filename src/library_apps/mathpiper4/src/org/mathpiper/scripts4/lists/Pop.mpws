%mathpiper,def="Pop"

Procedure("Pop",["stack", "index"])
{
  Local(result);
  result:=stack[index];
  Delete!(stack,index);
  result;
}

%/mathpiper



%mathpiper_docs,name="Pop",categories="Programming Procedures;Lists (Operations)"
*CMD Pop --- remove an element from a stack
*STD
*CALL
        Pop(stack, n)

*PARMS

{stack} -- a list (which serves as the stack container)

{n} -- index of the element to remove

*DESC

This is part of a simple implementation of a stack, internally
represented as a list. This command removes the element with index
"n" from the stack and returns this element. The top of the stack is
represented by the index 1. Invalid indices, for example indices
greater than the number of element on the stack, lead to an error.

*E.G.

In> stack := [];
Result: [];

In> Push(stack, x);
Result: [x];

In> Push(stack, x2);
Result: [x2,x];

In> Push(stack, x3);
Result: [x3,x2,x];

In> Pop(stack, 2);
Result: x2;

In> stack;
Result: [x3,x];

*SEE Push, PopFront, PopBack
%/mathpiper_docs