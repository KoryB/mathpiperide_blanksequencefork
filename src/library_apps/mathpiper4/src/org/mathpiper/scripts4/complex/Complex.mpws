%mathpiper,def="Complex"

0 # Complex(r_,i_Zero?) <-- r;
2 # Complex(Complex(r1_, i1_), i2_) <-- Complex(r1,i1+i2);
2 # Complex(r1_,Complex(r2_,i2_)) <-- Complex(r1-i2,r2);

6 # Complex(Undefined, x_) <-- Undefined;
6 # Complex(x_,Undefined) <-- Undefined;


/* Addition */

110 # Complex(r1_,i1_) + Complex(r2_,i2_) <-- Complex(r1+r2,i1+i2);
300 # Complex(r_,i_) + x_Constant? <-- Complex(r+x,i);
300 # x_Constant? + Complex(r_,i_) <-- Complex(r+x,i);

110 # - Complex(r_,i_) <-- Complex(-r,-i);

300 # Complex(r_,i_) - x_Constant? <-- Complex(r-x,i);
300 # x_Constant? - Complex(r_,i_) <-- Complex((-r)+x,-i);
111 # Complex(r1_,i1_) - Complex(r2_,i2_) <-- Complex(r1-r2,i1-i2);

/* Multiplication */
110 # Complex(r1_,i1_) * Complex(r2_,i2_) <-- Complex(r1*r2-i1*i2,r1*i2+r2*i1);
/* right now this is slower than above
110 # Complex(r1_,i1_) * Complex(r2_,i2_) <--
[        // the Karatsuba trick
        Local(A,B);
        A:=r1*r2;
        B:=i1*i2;
        Complex(A-B,(r1+i1)*(r2+i2)-A-B);
];
*/


// Multiplication in combination with complex numbers in the light of infinity
250 # Complex(r_Zero?,i_) * x_Infinity? <-- Complex(0,i*x);
250 # Complex(r_,i_Zero?) * x_Infinity? <-- Complex(r*x,0);
251 # Complex(r_,i_) * x_Infinity? <-- Complex(r*x,i*x);

250 # x_Infinity? * Complex(r_Zero?, i_) <-- Complex(0,i*x);
250 # x_Infinity? * Complex(r_,i_Zero?) <-- Complex(r*x,0);
251 # x_Infinity? * Complex(r_,i_) <-- Complex(r*x,i*x);


300 # Complex(r_,i_) * y_Constant? <-- Complex(r*y,i*y);
300 # y_Constant? * Complex(r_,i_) <-- Complex(r*y,i*y);

330 # Complex(r_,i_) * (y_Constant? / z_) <-- (Complex(r*y,i*y))/z;
330 # (y_Constant? / z_) * Complex(r_,i_) <-- (Complex(r*y,i*y))/z;


110 # x_Constant? / Complex(r_,i_) <-- (x*Conjugate(Complex(r,i)))/(r^2+i^2);


300 # Complex(r_,i_) / y_Constant? <-- Complex(r/y,i/y);

110 # (x_ ^ Complex(r_,i_)) <-- Exp(Complex(r,i)*Ln(x));


110 # (Complex(r_,i_) ^ x_RationalOrNumber?)::(Not?(Integer?(x))) <-- Exp(x*Ln(Complex(r,i)));

// This is commented out because it used PowerN so (2*I)^(-10) became a floating-point number. Now everything is handled by binary algorithm below
//120 # Complex(r_Zero?,i_) ^ n_Integer? <-- [1,I,-1,-I][1+Modulo(n,4)] * i^n;

123 # Complex(r_, i_) ^ n_NegativeInteger? <-- 1/Complex(r, i)^(-n);

124 # Complex(r_, i_) ^ (p_Zero?) <-- 1;        // cannot have Complex(0,0) here

125 # Complex(r_, i_) ^ n_PositiveInteger? <--
{
        // use binary method
        Local(result, x);
        x:=Complex(r,i);
        result:=1;
        While(n >? 0)
        {
                If((BitAnd(n,1)) =? 1)
                {
                  result := result*x;
                }
                x := x*x;
                n := n>>1;
        }
        result;
}


/*[        // this method is disabled b/c it suffers from severe roundoff errors
  Local(rr,ii,count,sign);
  rr:=r^n;
  ii:=0;
  For(count:=1,count<=?n,count:=count+2) [
    sign:=Decide(Zero?(Modulo(count-1,4)),1,-1);
    ii:=ii+sign*BinomialCoefficient(n,count)*i^count*r^(n-count);
    Decide(count<?n,
      rr:=rr-sign*BinomialCoefficient(n,count+1)*i^(count+1)*r^(n-count-1));
  ];
  Complex(rr,ii);
];
*/

%/mathpiper



%mathpiper_docs,name="Complex",categories="Mathematics Procedures;Numbers (Complex)"
*CMD Complex --- construct a complex number
*STD
*CALL
        Complex(r, c)

*PARMS

{r} -- real part

{c} -- imaginary part

*DESC

This procedure represents the complex number "r + I*c", where "I"
is the imaginary unit. It is the standard representation used in MathPiper
to represent complex numbers. Both "r" and "c" are supposed to be
real.

Note that, at the moment, many procedures in MathPiper assume that all
numbers are real unless it is obvious that it is a complex
number. Hence {Im(Sqrt(x))} evaluates to {0} which is only true for nonnegative "x".

*E.G.

In> I
Result: Complex(0,1);

In> 3+4*I
Result: Complex(3,4);

In> Complex(-2,0)
Result: -2;

*SEE Re, Im, I, Abs, Arg
%/mathpiper_docs





%mathpiper,name="Complex",subtype="automatic_test"

/* todo:tk:commenting out for the minimal version of the scripts.
Verify( Limit(z,2*I) (I*z^4+3*z^2-10*I), Complex(-12,6) );
KnownFailure( (Limit(n,Infinity) (n^2*I^n)/(n^3+1)) =? 0 );
Verify( Limit(n,Infinity) n*I^n, Undefined );
*/

Verify(1/I, -I);
Verify(I^2, -1);
Verify(2/(1+I), 1-I);
Verify(I^3, -I);
Verify(I^4, 1);
Verify(I^5, I);
Verify(1^I, 1);
Verify(0^I, Undefined);
Verify(I^(-I), Exp(Pi/2));
Verify((1+I)^33, 65536+I*65536);
Verify((1+I)^(-33), (1-I)/131072);
//Verify(Exp(I*Pi), -1);
//TestMathPiper((a_+b_*I)*(c_+d_*I), (a_*c_-b_*d_)+I*(a_*d_+b_*c_));
Verify(Ln(-1), I*Pi);
//Verify(Ln(3+4*I), Ln(5)+I*ArcTan(4/3));

Verify(Re(2*I-4), -4);
Verify(Im(2*I-4), 2);

%/mathpiper