%mathpiper,def="Verbose;Verbose?;VerboseOn;VerboseOff"

LocalSymbols(verbose) {

  Assign(verbose,False);


  Procedure("Verbose",["aNumberBody"])
  {
    Local(prevVerbose,result);
    Assign(prevVerbose,verbose);
    Assign(verbose,True);
    Assign(result,Eval(aNumberBody));
    Assign(verbose,prevVerbose);
    result;
  }


  Procedure("Verbose?",[]) verbose;
  
  Procedure("VerboseOn",[]) Assign(verbose,True);
  
  Procedure("VerboseOff",[]) Assign(verbose,False);
}

HoldArgument("Verbose","aNumberBody");
UnFence("Verbose",1);

%/mathpiper





%mathpiper_docs,name="Verbose;Verbose?",categories="Programming Procedures;Input/Output"
*CMD Verbose, Verbose? --- set verbose output mode
*STD
*CALL
        Verbose(expression)
        Verbose?()

*PARMS

[expression] -- expression to be evaluated in verbose mode

*DESC

The procedure {Verbose(expression)} will evaluate the expression in
verbose mode. Various parts of MathPiper can show extra information
about the work done while doing a calculation when using {Verbose}.

In verbose mode, {Verbose?()} will return {True}, otherwise
it will return {False}.

VerboseOn() turns verbose mode on, and VerboseOff() turns verbose mode off.

*E.G. notest

In> OldSolve([x+2==0],[x])
Result: [[-2]];

In> Verbose(OldSolve([x+2==0],[x]))
        Entering OldSolve
        From  x+2==0  it follows that  x  = -2
           x+2==0  simplifies to  True
        Leaving OldSolve
Result: [[-2]];

In> Verbose?()
Result: False

In> Verbose(Verbose?())
Result: True

*SEE Echo, NM
%/mathpiper_docs