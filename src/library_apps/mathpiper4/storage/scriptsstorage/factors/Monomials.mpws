%mathpiper,def="Monomials;CanBeMonomial;Monomial?;FactorsMonomial"

//Retract("CanBeMonomial",*);
//Retract("Monomial?",*);
//Retract("FactorsMonomial",*);

10 # CanBeMonomial(_expr)<--Not? (HasFunction?(expr,ToAtom("+")) Or? HasFunction?(expr,ToAtom("-")));

10 # Monomial?(expr_CanBeMonomial) <-- 
{
    Local(r);
    Decide( RationalFunction?(expr),
        r := (VarList(Denominator(expr)) =? []),
        r := True
    );
};

15 # Monomial?(_expr) <-- False;


10 # FactorsMonomial(expr_Monomial?) <--
{
  Decide(InVerboseMode(),Tell("FactorsMonomial",expr));
  Local(den,num,Ns,flat,prod,quot,result,f,ff);
  Decide( RationalFunction?(expr),
    {
      den := Denominator(expr);
      num := Flatten(Numerator(expr),"*");
    },
    {
      den := 1;
      num := Flatten(expr,"*");
    }
  );
  Decide(InVerboseMode(),Tell("     ",[num,den]));
  Ns  := Select(num, "Complex?");
  Decide(InVerboseMode(),Tell("     ",Ns));
  Decide( Ns =? [],
       Decide( den !=? 1, DestructiveInsert(num,1,1/den)),
       DestructiveReplace(num,Find(num,Ns[1]),Ns[1]/den)
  );
  Decide(InVerboseMode(),Tell("     ",num));
  result := [];
  ForEach(f,num)
  {
      Decide( Complex?(f), 
          Append!(result,[(f),1]),
          Decide( Atom?(f),
              Append!(result,[f,1]),
              Append!(result,DestructiveDelete(FunctionToList(f),1))
          )
      );
  };
  result;
};

%/mathpiper







%mathpiper_docs,name="Monomial?",categories="Programming Functions;Predicates"
*CMD Monomial? --- determine if [expr] is a Monomial
*STD
*CALL
        Monomial?(expr)

*PARMS

{expr} -- an expression

*DESC

This function returns {True} if {expr} satisfies the definition of a {Monomial}.
Otherwise, {False}.
A {Monomial} is defined to be a single term, consisting of a product of numbers
and variables.

*E.G.

In> Monomial?(24)
Result: True


In> Monomial?(24*a*x^2*y^3)
Result: True


In> Monomial?(24*a*x^2*y^3/15)
Result: True


In> Monomial?(24*a*x^2*y^3/15+1)
Result: False
    
%/mathpiper_docs



%mathpiper_docs,name="FactorsMonomial",categories="Mathematics Functions;Polynomials (Operations)"
*CMD FactorsMonomial --- factorization of a monomial expression
*STD
*CALL
        FactorsMonomial(expr)

*PARMS

{expr} -- an expression representing a Monomial

*DESC

This function decomposes the {expr} into a product of numbers and variables 
raised to various powers.

The factorization is returned as a list of pairs.  The first member of
each pair is the factor (a number or a variable name), while the second
member is an integer denoting the power to which this factor should be
raised.

Thus, the factorization  
$expr = p1^n1 * ... * p9^n9$ 
is returned as {[(p1,n1), ..., (p9,n9)]}.

If {expr} is not a monomial, the function returns unevaluated.

NOTE: numerical factors are not decomposed into their prime factorization.

*E.G.


In> FactorsMonomial(24)
Result: [[24,1]]


In> FactorsMonomial(24/15)
Result: [[8/5,1]]


In> FactorsMonomial(24*a*x^2*y^3)
Result: [[24,1],[a,1],[x,2],[y,3]]


In> FactorsMonomial(24*a*x^2*y^3/15)
Result: [[8/5,1],[a,1],[x,2],[y,3]]


In> FactorsMonomial(24*a*x^2*y^3/15+1)
Result: FactorsMonomial((24*a*x^2*y^3)/15+1)
    
%/mathpiper_docs



