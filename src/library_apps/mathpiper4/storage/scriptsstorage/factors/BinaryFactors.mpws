%mathpiper,def="BinaryFactors;LastCoef"

LocalSymbols(lastcoef,OrdBuild, AddFoundSolutionSingle , AddFoundSolution, Fct, MkfactD,p)
{

LastCoef(_vector,_p) <--
{
  Local(n);
  n:=Length(vector);
  Add(vector*p^(0 .. (n-1)));
};

/*
Ord(vector,q):=
[
  Local(n);
  n:=Length(vector);
  q*Coef(Simplify(LastCoef(vector,p+q)-LastCoef(vector,p)),q,1);
];
*/

OrdBuild(vector,q):=
{
  Local(i,result,n);
  Assign(i,2);
  Assign(result, 0);
  Assign(n, Length(vector));
  While (i<=?n)
  {
    Assign(result,result+(i-1)*vector[i]*p^(i-2));
    Assign(i, i+2);
  };
  q*result;
};


Function(AddFoundSolutionSingle,[p])
{
  Local(calc);
//  Decide( Not? Contains?(result,p),
//  [
    Assign(calc, Eval(lastcoef));
    Decide(Equal?(calc, 0),
    {
      Local(newlist,count,root);
      count:=0;
      root := p;
      Local(rem);

      rem:=[-root,1];
      [testpoly,rem]:=MkfactD(testpoly,rem);

      rem:=[-root,1];
      [newlist,rem]:=MkfactD(poly,rem);
      While (rem =? [])
      {
        count++;
        Assign(poly,newlist);
        rem:=[-root,1];
        [newlist,rem]:=MkfactD(poly,rem);
      };

      Local(lgcd,lc);
      Assign(lgcd,Gcd([andiv,an,root]));
      Assign(lc,Quotient(an,lgcd));
      Assign(result,[var+ (-(Quotient(root,lgcd)/lc)),count]~result);
      Assign(andiv,Quotient(andiv,lgcd^count));
      Assign(anmul,anmul*lc^count);

//      factor:=(x-root);
//      Assign(result,[factor,count]:result);

      Local(p,q);
      Assign(lastcoef, LastCoef(testpoly,p));
      Assign(ord, OrdBuild(testpoly,q));
    });
//  ]);
};
UnFence(AddFoundSolutionSingle,1);

Function(AddFoundSolution,[p])
{
  AddFoundSolutionSingle(p);
  AddFoundSolutionSingle(-2*q+p);
};
UnFence(AddFoundSolution,1);

Function(Fct,[poly,var])
{
  Local(maxNrRoots,result,ord,p,q,accu,calc,twoq,mask);

  Local(gcd);
  {
    Assign(gcd,Gcd(poly));
    Decide(poly[Length(poly)] <? 0,Assign(gcd, gcd * -1));
    Assign(poly,poly/gcd);
  };

  Local(unrat);
  Assign(unrat,Lcm(MapSingle("Denominator",poly)));
  Assign(poly,unrat*poly);

  Local(origdegree);
  Assign(origdegree,Length(poly)-1);

  Local(an,andiv,anmul);
  Assign(an,poly[Length(poly)]);
  Assign(poly,poly* (an^((origdegree-1) .. -1)));
  Assign(andiv,an^(origdegree-1));
  Assign(anmul,1);

  Local(leadingcoef,lowestcoef);
  Assign(leadingcoef,poly[Length(poly)]);
  {
    Local(i);
    Assign(i,1);
    Assign(lowestcoef,Abs(poly[i]));
    While (lowestcoef =? 0 And? i<=?Length(poly))
    {
      Assign(i,i+1);
      Assign(lowestcoef,Abs(poly[i]));
    };
  };
  // testpoly is the square-free version of the polynomial, used for finding
  // the factors. the original polynomials is kept around to find the
  // multiplicity of the factor.
  Local(testpoly);
//  Assign(testpoly,Mkc(Quotient(polynom,Monic(Gcd(polynom,Deriv(var)polynom))),var));
  Local(deriv);
  // First determine a derivative of the original polynomial
  deriv:=Rest(poly);
  {
    Local(i);
    For (i:=1,i<=?Length(deriv),i++)
    {
      deriv[i] := deriv[i]*i;
    };
//    Echo("POLY = ",poly);
//    Echo("DERIV = ",deriv);
  };
  {
    Local(q,r,next);
    q:=poly;
    r:=deriv;
    While(r !=? [])
    {
//Echo(q,r);
      next := MkfactD(q,r)[2];
      q:=r;
      r:=next;
    };
    // now q is the gcd of the polynomial and its first derivative.

    // Make it monic
    q:=q/q[Length(q)];
    testpoly:=MkfactD(poly,q)[1];
//Echo("TESTPOLY = ",testpoly);
  };

//  Assign(testpoly,poly); //@@@

  Assign(maxNrRoots,Length(testpoly)-1);
  Assign(result, []);

  Assign(lastcoef, LastCoef(testpoly,p));
  Assign(ord, OrdBuild(testpoly,q));

  Assign(accu,[]);
  Assign(q,1);
  Assign(twoq,MultiplyN(q,2));
  Assign(mask,AddN(twoq,MathNegate(1)));
  If(Even?(testpoly[1]))
  {
    Assign(accu,0~accu);
    AddFoundSolutionSingle(0);
  };
  Assign(p,1);
  Assign(calc, Eval(lastcoef));
  Decide(Even?(calc),
  {
    Assign(accu,1~accu);
    AddFoundSolution(1);
  });
  Assign(q,twoq);
  Assign(twoq,MultiplyN(q,2));
  Assign(mask,AddN(twoq,MathNegate(1)));
  While(Length(result)<?maxNrRoots And? Length(accu)>?0 And? q<=?Abs(testpoly[1]))
  {
    Local(newaccu);
    Assign(newaccu,[]);
    ForEach(p,accu)
    {
      Assign(calc,Eval(lastcoef));
      Decide(LessThan?(calc,0),
        Assign(calc, AddN(calc,MultiplyN(twoq,QuotientN(AddN(MathNegate(calc),twoq),twoq))))
         );
      Assign(calc, BitAnd(calc, mask));
      Decide( Equal?(calc, 0),
      {
        Assign(newaccu, p~newaccu);
        AddFoundSolutionSingle(-2*q+p);
      });
      Assign(calc, AddN(calc, Eval(ord)));
      Decide(LessThan?(calc,0),
        Assign(calc, AddN(calc,MultiplyN(twoq,QuotientN(AddN(MathNegate(calc),twoq),twoq))))
         );
      Assign(calc, BitAnd(calc, mask));
      Decide( Equal?(calc, 0),
      {
        Assign(newaccu, AddN(p,q)~newaccu);
        AddFoundSolution(AddN(p,q));
      });
    };
    Assign(accu, newaccu);
    Assign(q,twoq);
    Assign(twoq,MultiplyN(q,2));
    Assign(mask,AddN(twoq,MathNegate(1)));

//Echo("q = ",q);
//Echo("Length is",Length(accu),"accu = ",accu);
//Echo("result = ",result);
  };

  // If the polynom is not one, it is a polynomial which is not reducible any further
  // with this algorithm, return as is.
  Assign(poly,poly*an^(0 .. (Length(poly)-1)));
  Assign(poly,gcd*anmul*poly);
  //TODO had to add this if statement, what was andiv again, and why would it become zero? This happens with for example Factor(2*x^2)
  Decide(Not? Zero?(unrat * andiv ),Assign(poly,poly/(unrat * andiv )));
  Decide(poly !=? [1],
  {
    result:=[(Add(poly*var^(0 .. (Length(poly)-1)))),1]~result;
  });
  result;
};



BinaryFactors(expr):=
{
  Local(result,uni,coefs);
  uni:=MakeUni(expr,VarList(expr)[1]);
  uni:=FunctionToList(uni);
  coefs:=uni[4];
  coefs:=Concat(ZeroVector(uni[3]),coefs);
  result:=Fct(coefs,uni[2]);
//  Echo(result,list);
//  Echo((Add(list*x^(0 .. (Length(list)-1)))));
//  Product(x-result)*(Add(list*x^(0 .. (Length(list)-1))));
  result;
};



MkfactD(numer,denom):=
{
  Local(q,r,i,j,ln,ld,nq);
  DropEndZeroes(numer);
  DropEndZeroes(denom);
  Assign(numer,Reverse(numer));
  Assign(denom,Reverse(denom));
  Assign(ln,Length(numer));
  Assign(ld,Length(denom));
  Assign(q,FillList(0,ln));
  Assign(r,FillList(0,ln));

  Assign(i,1);
  Decide(ld>?0,
  {
    While(Length(numer)>=?Length(denom))
    {
      Assign(nq,numer[1]/denom[1]);
      q[ln-(Length(numer)-ld)] := nq;
      For(j:=1,j<=?Length(denom),j++)
      {
        numer[j] := (numer[j] - nq*denom[j]);
      };
      r[i] := r[1] + numer[1];

      Assign(numer, Rest(numer));
      i++;
    };
  });
  For(j:=0,j<?Length(numer),j++)
  {
    r[i+j] := r[i+j] + numer[j+1];
  };
  Assign(q,Reverse(q));
  Assign(r,Reverse(r));
  DropEndZeroes(q);
  DropEndZeroes(r);
  [q,r];
};

}; //LocalSymbols


%/mathpiper





%mathpiper_docs,name="BinaryFactors",categories="Mathematics Functions",access="undocumented"
*CMD BinaryFactors --- *** UNDOCUMENTED ***

*CALL
        ?

*PARMS

?

*DESC

?

%/mathpiper_docs







%mathpiper,name="BinaryFactors",subtype="automatic_test"


TestPoly(poly,requiredResult):=
{
//Echo(poly);
  Local(realResult);
  realResult:=BinaryFactors(poly);
  Verify(Length(realResult),Length(requiredResult));

//Echo(requiredResult,realResult);
  Local(intersection);
  intersection:=[];
  ForEach(item1,requiredResult)
    ForEach(item2,realResult)
    {
      Decide(Simplify(item1-item2) =? [0,0],
        intersection := (item1~intersection));
    };
  Verify(Length(realResult),Length(intersection/*Intersection(requiredResult,realResult)*/));
  Verify(Simplify(poly-FW(realResult)),0);
};

// Simple factorizations
TestPoly((x+1)*(x-1),[[x+1,1],[x-1,1]]);

// Simple with multiple factors
TestPoly((x+1)^2,[[x+1,2]]);

// Test: term with lowest power not zero power
TestPoly(x^2*(x+1)*(x-1),[[x,2],[x+1,1],[x-1,1]]);
TestPoly(x^3*(x+1)*(x-1),[[x,3],[x+1,1],[x-1,1]]);

// Variable different from x
TestPoly((y+1)*(y-1),[[y+1,1],[y-1,1]]);

// Test from Wester 1994 test
TestPoly(Differentiate(x)(x+1)^20,[[20,1],[x+1,19]]);

// From regression test, and verify that polys with unfactorizable parts works
TestPoly((x^6-1),[[x^4+x^2+1,1],[x+1,1],[x-1,1]]);

// Non-monic polynomials
TestPoly((x+13)^2*(3*x-5)^3,[[27,1],[x+13,2],[x-5/3,3]]);
TestPoly((x+13)^2*(4*x-5)^3,[[64,1],[x+13,2],[x-5/4,3]]);

// Heavy: binary coefficients
TestPoly((x+1024)*(x+2048),[[x+1024,1],[x+2048,1]]);
TestPoly((x+1024)^2*(x+2048)^3,[[x+1024,2],[x+2048,3]]);
TestPoly((16*x+1024)*(x+2048),[[16,1],[x+64,1],[x+2048,1]]);
TestPoly((x+1024)*(x+2047),[[x+1024,1],[x+2047,1]]);
TestPoly((x+1024)*(x+2049),[[x+1024,1],[x+2049,1]]);

TestPoly((x+1024)*(x-2047),[[x+1024,1],[x-2047,1]]);
TestPoly((x-1024)*(x+2047),[[x-1024,1],[x+2047,1]]);
TestPoly((x-1024)*(x-2047),[[x-1024,1],[x-2047,1]]);

// Rational coefficients
TestPoly((x+4/7)*(x-5/9),[[x+4/7,1],[x-5/9,1]]);

// More than two factors ;-)
TestPoly((x+1)*(x-2)*(x+3)*(x-4)*(x+5)*(x-6),[[x+1,1],[x-2,1],[x+3,1],[x-4,1],[x+5,1],[x-6,1]]);


%/mathpiper