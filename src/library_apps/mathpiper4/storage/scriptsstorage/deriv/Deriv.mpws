%mathpiper,def="Deriv"

5 # (Deriv(var_,1)func_) <-- Deriv(var)func;
5 # (Deriv(var_,0)func_) <-- func;
10 # (Deriv(var_,n_PositiveInteger?)func_) <-- Deriv(var)Deriv(var,n-1)func;
10 # (Deriv(var_,n_NegativeInteger?)func_) <-- Check(0, "Math", "Negative derivative");


// Need to clean out Sec(x) and friends
0 # (Deriv(var_) (var_)) <-- 1;
1 # (Deriv(var_)func_Atom?) <-- 0;
2 # (Deriv(var_)x_ + y_) <--  (Deriv(var)x) + (Deriv(var)y);
2 # (Deriv(var_)- (x_) ) <-- -Deriv(var)x;
2 # (Deriv(var_)x_ - y_) <--  (Deriv(var)x) - (Deriv(var)y);
2 # (Deriv(var_)x_ * y_) <-- (x*Deriv(var)y) + (Deriv(var)x)*y;
2 # (Deriv(var_)Sin(x_)) <--  (Deriv(var)x)*Cos(x);
2 # (Deriv(var_)Sinh(x_))<--  (Deriv(var)x)*Cosh(x);
2 # (Deriv(var_)Cosh(x_))<--  (Deriv(var)x)*Sinh(x);
2 # (Deriv(var_)Cos(x_)) <-- -(Deriv(var)x)*Sin(x);
2 # (Deriv(var_)Csc(x_)) <--  -(Deriv(var)x)*Csc(x)*Cot(x);
2 # (Deriv(var_)Csch(x_)) <-- -(Deriv(var)x)*Csch(x)*Coth(x);
2 # (Deriv(var_)Sec(x_)) <--  (Deriv(var)x)*Sec(x)*Tan(x);
2 # (Deriv(var_)Sech(x_)) <-- -(Deriv(var)x)*Sech(x)*Tanh(x);
2 # (Deriv(var_)Cot(x_)) <--  -(Deriv(var)x)*Csc(x)^2;
2 # (Deriv(var_)Coth(x_)) <--  (Deriv(var)x)*Csch(x)^2;

2 # (Deriv(var_)Tan(x_)) <-- ((Deriv(var) x) / (Cos(x)^2));
2 # (Deriv(var_)Tanh(x_)) <-- (Deriv(var)x)*Sech(x)^2;

2 # (Deriv(var_)Exp(x_)) <--  (Deriv(var)x)*Exp(x);

// When dividing by a constant, this is faster
2 # (Deriv(var_)(x_ / y_))::(FreeOf?(var,y)) <-- (Deriv(var) x) / y;
3 # (Deriv(var_)(x_ / y_)) <--
    (y* (Deriv(var) x) - x* (Deriv(var) y))/ (y^2);

2 # (Deriv(var_)Ln(x_)) <-- ((Deriv(var) x) / x);
2 # (Deriv(var_)(x_ ^ n_))::(RationalOrNumber?(n) Or? FreeOf?(var, n)) <--
    n * (Deriv(var) x) * (x ^ (n - 1));

//2 # (Deriv(var_)(Abs(x_)))  <-- Sign(x)*(Deriv(var)x);
2 # (Deriv(var_)(Abs(x_)))  <-- (x/Abs(x))*(Deriv(var)x);
2 # (Deriv(var_)(Sign(x_))) <-- 0;

2 # (Deriv(var_)(If(cond_)(body_))) <--
        ListToFunction([ToAtom("If"),cond,Deriv(var)body]);
2 # (Deriv(var_)((left_) Else (right_))) <--
        ListToFunction([ToAtom("Else"), (Deriv(var)left), (Deriv(var)right) ] );

3 # (Deriv(var_)(x_ ^ n_)) <-- (x^n)*Deriv(var)(n*Ln(x));

2 # (Deriv(var_)ArcSin(x_)) <-- (Deriv(var) x)/Sqrt(1 - (x^2));
2 # (Deriv(var_)ArcCos(x_)) <-- -(Deriv(var)x)/Sqrt(1 - (x^2));
2 # (Deriv(var_)ArcTan(x_)) <-- (Deriv(var) x)/(1 + x^2);

2 # (Deriv(var_)ArcSinh(x_)) <-- (Deriv(var) x)/Sqrt((x^2) + 1);
2 # (Deriv(var_)ArcCosh(x_)) <-- (Deriv(var) x)/Sqrt((x^2) - 1);
2 # (Deriv(var_)ArcTanh(x_)) <-- (Deriv(var) x)/(1 - x^2);

2 # (Deriv(var_)Sqrt(x_)) <-- ((Deriv(var)x)/(2*Sqrt(x)));
2 # (Deriv(var_)Complex(r_,i_)) <-- Complex(Deriv(var)r,Deriv(var)i);

LocalSymbols(var,var2,a,b,y){
   2 # (Deriv(var_)Integrate(var_)(y_)) <-- y;
   2 # (Deriv(var_)Integrate(var2_,a_,b_)(y_FreeOf?(var))) <--
         (Deriv(var)b)*(y Where var2 == b) -
         (Deriv(var)a)*(y Where var2 == a);
   3 # (Deriv(var_)Integrate(var2_,a_,b_)(y_)) <--
         (Deriv(var)b)*(y Where var2 == b) -
         (Deriv(var)a)*(y Where var2 == a) +
         Integrate(var2,a,b) Deriv(var) y;
 };



2 # (Deriv(var_)func_List?)::(Not?(List?(var))) <--
    Map("Deriv",[FillList(var,Length(func)),func]);


2 # (Deriv(var_)UniVariate(var_,first_,coefs_)) <--
{
  Local(result,m,i);
  result:=FlatCopy(coefs);
  m:=Length(result);
  For(i:=1,i<=?m,i++)
  {
    result[i] := result[i] * (first+i-1);
  };
  UniVariate(var,first-1,result);
};

%/mathpiper





%mathpiper_docs,name="Deriv",categories="Mathematics Functions",access="undocumented"
*CMD Deriv --- *** UNDOCUMENTED ***

*CALL
        ?

*PARMS

?

*DESC

?

%/mathpiper_docs





%mathpiper,name="Deriv",subtype="automatic_test"

TestMathPiper(Deriv(x)Ln(x),1/x);
TestMathPiper(Deriv(x)Exp(x),Exp(x));
TestMathPiper(Deriv(x)(x^4+x^3+x^2+x+1),4*x^3+3*x^2+2*x+1);
TestMathPiper(Deriv(x)Sin(x),Cos(x));
TestMathPiper(Deriv(x)Cos(x),-Sin(x));
TestMathPiper(Deriv(x)Sinh(x),Cosh(x));
TestMathPiper(Deriv(x)Cosh(x),Sinh(x));
TestMathPiper(Deriv(x)ArcCos(x),-1/Sqrt(1-x^2));
TestMathPiper(Deriv(x)ArcSin(x),1/Sqrt(1-x^2));
TestMathPiper(Deriv(x)ArcTan(x),1/(x^2+1));
TestMathPiper(Deriv(x)Sech(x),-Sech(x)*Tanh(x));

%/mathpiper