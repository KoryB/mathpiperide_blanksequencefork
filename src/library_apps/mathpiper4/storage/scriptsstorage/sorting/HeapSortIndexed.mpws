%mathpiper,def="HeapSortIndexed"

//Retract("HeapSortIndexed",*);
//Retract("HeapSortIndexed1",*);

HeapSortIndexed( L_List?, _compare ) <-- HeapSortIndexed1( FlatCopy(L), compare );

HeapSortIndexed1( L_List?, _compare ) <--
{
  Local(list,indices,pairs,sortedPairs);

  list    := FlatCopy(L);
  indices := BuildList(i,i,1,Length(list),1);
  pairs   := Transpose(list~[indices]);
  SortedPairs := [];
  sortedPairs := Decide( Apply(compare,[3,5]), 
     HeapSort( pairs, Lambda([X,Y],X[1] <? Y[1]) ),
     HeapSort( pairs, Lambda([X,Y],X[1] >? Y[1]) )
  );
  Transpose(sortedPairs);
};

%/mathpiper




%mathpiper_docs,name="HeapSortIndexed",categories="Programming Functions;Sorting"
*CMD HeapSortIndexed --- sort a list
*STD
*CALL
        HeapSortIndexed(list, compare)

*PARMS

{list} -- list to sort

{compare} -- function used to compare elements of {list}

*DESC

This command returns {list} after it is sorted using {compare} to
compare elements. The procedure {compare} should accept two arguments,
which will be elements of {list}, and compare them. It should return
{True} if in the sorted list the second argument
should come after the first one, and {False}
otherwise.  The procedure {compare} can either be a string which
contains the name of a function or a pure function.

The procedure {HeapSortIndexed} uses a recursive algorithm "heapsort" and is much
faster than {BubbleSortIndexed} for large lists. The sorting time is proportional 
to $n*Ln(n)$, where $n$ is the length of the list.

An {IndexedSort}, performs the same action as a "regular" sort, but also
return an Index list, which gives the order that the sorted items 
had in the original list before sorting.

The original {list} is left unmodified.

*E.G.
In> HeapSortIndexed([-1,88,4,7,23,53,-2,1], ">?")
Result: [[88,53,23,7,4,1,-1,-2],[2,6,5,4,3,8,1,7]]

In> HeapSortIndexed([-1,88,4,7,23,53,-2,1], Lambda([x,y],x<?y))
Result: [[-2,-1,1,4,7,23,53,88],[7,1,8,3,4,5,6,2]]

*SEE BubbleSort, Lambda
%/mathpiper_docs

