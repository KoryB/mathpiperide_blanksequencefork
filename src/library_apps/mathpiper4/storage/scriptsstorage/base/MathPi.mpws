%mathpiper,def="MathPi"

Defun("MathPi",[])
{
  // Newton's method for finding pi:
  // x[0] := 3.1415926
  // x[n+1] := x[n] + Sin(x[n])
  Local(initialPrec,curPrec,result,aPrecision);
  Assign(aPrecision,BuiltinPrecisionGet());
        Assign(initialPrec, aPrecision);        // target precision of first iteration, will be computed below
  Assign(curPrec, 40);  // precision of the initial guess
  Assign(result, 3.141592653589793238462643383279502884197169399);    // initial guess

        // optimize precision sequence
        While (GreaterThan?(initialPrec, MultiplyN(curPrec,3)))
  {
                Assign(initialPrec, FloorN(DivideN(AddN(initialPrec,2),3)));
  };
        Assign(curPrec, initialPrec);
  While (Not?(GreaterThan?(curPrec, aPrecision)))
  {
                 // start of iteration code
    // Get Sin(result)
    BuiltinPrecisionSet(curPrec);
    Assign(result,AddN(result,SinN(result)));
    // Calculate new result: result := result + Sin(result);
                // end of iteration code
                // decide whether we are at end of loop now
                Decide(Equal?(curPrec, aPrecision),        // if we are exactly at full precision, it's the last iteration
    {
                        Assign(curPrec, AddN(aPrecision,1));        // terminate loop
    },
    {
                        Assign(curPrec, MultiplyN(curPrec,3));        // precision triples at each iteration
                        // need to guard against overshooting precision
                         Decide(GreaterThan?(curPrec, aPrecision),
      {
                                Assign(curPrec, aPrecision);        // next will be the last iteration
      });
                });
  };
  BuiltinPrecisionSet(aPrecision);
  result;
};

%/mathpiper





%mathpiper_docs,name="MathPi",categories="Mathematics Functions",access="undocumented"
*CMD MathPi --- *** UNDOCUMENTED ***

*CALL
        ?

*PARMS

?

*DESC

?

%/mathpiper_docs