%mathpiper,def="InternalZetaNum;InternalZetaNum1;InternalZetaNum2;Zeta3"

/* def file definitions
InternalZetaNum
InternalZetaNum1
InternalZetaNum2
Zeta3
*/

/////////////////////////////////////////////////
/// Riemann's Zeta function
/////////////////////////////////////////////////

//Serge Winitzki

/// See: Bateman, Erdelyi: <i>Higher Transcendental Functions<i>, vol. 1;
/// P. Borwein, <i>An efficient algorithm for Riemann Zeta function<i> (1995).

/// Numerical computation of Zeta function using Borwein's "third" algorithm
/// The value of $n$ must be large enough to ensure required precision
/// Also $s$ must satisfy $Re(s)+n+1 > 0$
InternalZetaNum(_s, n_Integer?) <-- {
        Local(result, j, sign);
        Decide(InVerboseMode(), Echo(["InternalZetaNum: Borwein's method, precision ", BuiltinPrecisionGet(), ", n = ", n]));
        result := 0;
        sign := 1;        // flipping sign
        For(j:=0, j<=?2*n-1, j++)
        {        // this is suboptimal b/c we can compute the coefficients a lot faster in this same loop, but ok for now
                result := NM(result + sign*InternalZetaNumCoeffEj(j,n)/(1+j)^s );
                sign := -sign;
        };
        NM(result/(2^n)/(1-2^(1-s)));
};

/// direct method -- only good for large s
InternalZetaNum1(s, limit) := {
        Local(i, sum);
        Decide(InVerboseMode(), Echo(["InternalZetaNum: direct method (sum), precision ", BuiltinPrecisionGet(), ", N = ", limit]));
        sum := 0;
        limit := Ceil(NM(limit));
        For(i:=2, i<=?limit, i++) sum := sum+NM(1/PowerN(i, s));
//        sum := sum + ( NM( 1/PowerN(limit, s-1)) + NM(1/PowerN(limit+1, s-1)) )/2/(s-1);          // these extra terms don't seem to help much
        sum+1;        // add small terms together and then add 1
};
/// direct method -- using infinite product. For internal math, InternalZetaNum2 is faster for Bernoulli numbers > 250 or so.
InternalZetaNum2(s, limit) :=
{
        Local(i, prod);
        Decide(InVerboseMode(), Echo(["InternalZetaNum: direct method (product), precision ", BuiltinPrecisionGet(), ", N = ", limit]));
        prod := NM( (1-1/PowerN(2, s))*(1-1/PowerN(3,s)) );
        limit := Ceil(NM(limit));
        For(i:=5, i<=?limit, i:= NextPrime(i))
                prod := prod*NM(1-1/PowerN(i, s));
        1/prod;
};

/// Compute coefficients e[j] (see Borwein -- excluding (-1)^j )
InternalZetaNumCoeffEj(j,n) := {
        Local(k);
        2^n-Decide(j<?n,
                0,
                Sum(k,0,j-n,BinomialCoefficient(n,k))        // this is suboptimal but ok for now
        );
};

/// fast numerical calculation of Zeta(3) using a special series




Zeta3() :=
{
        Local(result, oldresult, k, term);
  NM({
    For(
    {
      k:=1;
      result := 1;
      oldresult := -1;
      term := 1;
    },
    oldresult!=?result,
    k++
    )
    {
      oldresult := result;
      term := -term * k^2 / ((2*k+1)*(2*k));
      result := result + term/(k+1)^2;
    };
    result := 5/4*result;
  }, BuiltinPrecisionGet()+IntLog(BuiltinPrecisionGet(),10)+1);

        result;
};


/// User interface for InternalZetaNum(s,n)
10 # InternalZetaNum(_s) _ (NM(s)=?0) <-- -0.5;
10 # InternalZetaNum(_s) _ (NM(s)=?1) <-- Infinity;
20 # InternalZetaNum(_s) <-- {
        Local(n, prec, result);
        prec := BuiltinPrecisionGet();
        Decide(        // use identity if s<1/2 to replace with larger s. Also must be sn!=0 or Else we get infinity * zero
                NM(Re(s)) <? 0.5,
                // call ourselves with a different argument
                {
                        Decide(InVerboseMode(), Echo(["InternalZetaNum: using s->1-s identity, s=", s, ", precision ", prec]));
                        result :=  2*Exp(InternalLnGammaNum(1-s)-(1-s)*Ln(2*InternalPi()))*Sin(InternalPi()*s/2) * InternalZetaNum(1-s);
                },
                // choose between methods
                Decide(NM(Re(s)) >? NM(1+(prec*Ln(10))/(Ln(prec)+0.1), 6),
                        {        // use direct summation
                                n:= NM(10^(prec/(s-1)), 6)+2;        // 2 guard terms
                                BuiltinPrecisionSet(prec+2);        // 2 guard digits
                                result := InternalZetaNum1(s, n);
                        },
                        {        // use InternalZetaNum(s, n)
                                n := Ceil( NM( prec*Ln(10)/Ln(8) + 2, 6 ) );        // add 2 digits just in case
                                BuiltinPrecisionSet(prec+2);        // 2 guard digits
                                result := InternalZetaNum(s, n);
                        }
                )
        );
        BuiltinPrecisionSet(prec);
        result;
};


%/mathpiper