//Copyright (C) 2008 Ted Kosan (license information is at the end of this document.)

package org.mathpiper.ide.plotterplugin;

/*
 * Plotter.java
 * part of the Plotter plugin for the jEdit text editor
 * Copyright (C) 2008 Ted Kosan

 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * $Id: Plotter.java 9481 2007-05-02 00:34:44Z k_satoda $
 */

// {{{ imports
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.gjt.sp.jedit.EBComponent;
import org.gjt.sp.jedit.EBMessage;
import org.gjt.sp.jedit.EditBus;
import org.gjt.sp.jedit.GUIUtilities;
import org.gjt.sp.jedit.View;
import org.gjt.sp.jedit.jEdit;
import org.gjt.sp.jedit.gui.DefaultFocusComponent;
import org.gjt.sp.jedit.gui.DockableWindowManager;
import org.gjt.sp.jedit.msg.PropertiesChanged;
import org.gjt.sp.util.Log;
import org.gjt.sp.util.StandardUtilities;


import javax.swing.JScrollPane;
import org.mathpiper.ui.gui.worksheets.GraphPanelController;
// }}}

// {{{ Plotter class
/**
 * 
 * Plotter - a dockable JPanel, a demonstration of a jEdit plugin.
 *
 */
public class Plotter extends javax.swing.JRootPane
	implements EBComponent, PlotterActions, DefaultFocusComponent {

	// {{{ Instance Variables
	//private static final long serialVersionUID = 6412255692894321789L;

	private String filename;

	private String defaultFilename;

	private View view;

	private boolean floating;
	
	


	// }}}

	// {{{ Constructor
	/**
	 * 
	 * @param view the current jedit window
	 * @param position a variable passed in from the script in actions.xml,
	 * 	which can be DockableWindowManager.FLOATING, TOP, BOTTOM, LEFT, RIGHT, etc.
	 * 	see @ref DockableWindowManager for possible values.
	 */
	public Plotter(View view, String position) {
		
		//super(new BorderLayout());
		
				//System.out.println("XXXXXXXXXXXXXXXXXXX Plotter.java initialized.");
		this.view = view;
		this.floating = position.equals(DockableWindowManager.FLOATING);




		if (floating)
			this.setPreferredSize(new Dimension(500, 250));


		double viewScale = 1.9;
		JPanel mathControllerPanel = new JPanel();
        mathControllerPanel.setLayout(new BorderLayout());
        final org.mathpiper.ui.gui.worksheets.PlotPanel plotPanel = new org.mathpiper.ui.gui.worksheets.PlotPanel(null, -1, viewScale, new java.util.HashMap());
        //plotPanel.addResponseListener(this);
        GraphPanelController mathPanelScaler = new GraphPanelController(plotPanel, viewScale);
        JScrollPane scrollPane = new JScrollPane(plotPanel,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPane.getVerticalScrollBar().setUnitIncrement(16);
        mathControllerPanel.add(scrollPane);
        mathControllerPanel.add(mathPanelScaler, BorderLayout.NORTH);

    	this.getContentPane().add(mathControllerPanel);
    	
    	JMenu fileMenu = new JMenu("File");
        JMenuItem saveAsImageAction = new JMenuItem();
        saveAsImageAction.setText("Save As Image");
        saveAsImageAction.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ae)
            {
                org.mathpiper.ui.gui.Utility.saveImageOfComponent(plotPanel);
            }
        });
        fileMenu.add(saveAsImageAction);
        JMenuBar menuBar = new JMenuBar();
        menuBar.add(fileMenu);
        this.setJMenuBar(menuBar);
    	
    	
		try{
			org.mathpiper.interpreters.Interpreter synchronousInterpreter = org.mathpiper.interpreters.Interpreters.getSynchronousInterpreter();
			org.mathpiper.lisp.Environment environment = synchronousInterpreter.getEnvironment();
			org.mathpiper.builtin.JavaObject javaObject = new org.mathpiper.builtin.JavaObject(plotPanel);
			environment.setLocalOrGlobalVariable(-1, "?plotter",  org.mathpiper.lisp.cons.BuiltinObjectCons.getInstance(environment, -1, javaObject), false, false, false);
			
			//geoGebraApplet.registerAddListener("PlotterAddListener");
			//geoGebraApplet.registerUpdateListener("PlotterUpdateListener");
			
			//System.out.println("HHHHHHHHHHHHHHHHHHH Plotter listeners registered.");
		}catch(Throwable e)
		{
			e.printStackTrace();
		}

	}//end constructor.
	// }}}

	//{{{
	//This fixes a problem where the look and feel does not display correctly.
	//For some reason, adjusting the split pane corrects the display.
	private void adjustSplitPane( java.awt.Container container )
	{
		if(container == null)
		{
			return;
		}

		java.awt.Component[] components = container.getComponents();

		if(components == null)
		{
			return;
		}

		for(int x = 0; x < components.length; x++)
		{
			if(components[x] instanceof javax.swing.JSplitPane)
			{
				javax.swing.JSplitPane splitPane = (javax.swing.JSplitPane) components[x];
				splitPane.setDividerLocation(100);
				return;
			}
			else if (components[x] instanceof java.awt.Container)
			{

				adjustSplitPane((java.awt.Container) components[x]);
			}
		}
	}//end
	//}}}

	// {{{ Member Functions




	// {{{ focusOnDefaultComponent
	public void focusOnDefaultComponent() {
		//textArea.requestFocus();
	}
	// }}}

	// {{{ getFileName
	public String getFilename() {
		return filename;
	}
	// }}}

	// EBComponent implementation

	// {{{ handleMessage
	public void handleMessage(EBMessage message) {
		if (message instanceof PropertiesChanged) {
			propertiesChanged();
		}
	}
	// }}}

	// {{{ propertiesChanged
	private void propertiesChanged() {
		/*String propertyFilename = jEdit
				.getProperty(PlotterPlugin.OPTION_PREFIX + "filepath");
		if (!StandardUtilities.objectsEqual(defaultFilename, propertyFilename)) {
			saveFile();
			toolPanel.propertiesChanged();
			defaultFilename = propertyFilename;
			filename = defaultFilename;
			readFile();
	}
		Font newFont = PlotterOptionPane.makeFont();
		if (!newFont.equals(textArea.getFont())) {
			textArea.setFont(newFont);
	}*/
	}//end method.


	// }}}

	// These JComponent methods provide the appropriate points
	// to subscribe and unsubscribe this object to the EditBus.

	// {{{ addNotify
	public void addNotify() {
		super.addNotify();
		EditBus.addToBus(this);
	}
	// }}}

	// {{{ removeNotify
	public void removeNotify() {
		//saveFile();
		super.removeNotify();
		EditBus.removeFromBus(this);
	}
	// }}}

	// PlotterActions implementation

	// {{{
	public void reset() {
		//ggbPanel.getPlotterAPI().setXML("<?xml version=\"1.0\" encoding=\"utf-8\"?> <plotter format=\"2.5\"> </plotter>");
	}
	// }}}

	// {{{ chooseFile
	public void chooseFile() {
		/*
		String[] paths = GUIUtilities.showVFSFileDialog(view, null,
				JFileChooser.OPEN_DIALOG, false);
		if (paths != null && !paths[0].equals(filename)) {
			saveFile();
			filename = paths[0];
			toolPanel.propertiesChanged();
			readFile();
	}
		*/
	}
	// }}}

	// {{{ copyToBuffer
	public void copyToBuffer() {
		/*
		jEdit.newFile(view);
		view.getEditPane().getTextArea().setText(textArea.getText());
		*/
	}//end method.
	// }}}

	// }}}
}//end class.
// }}}

/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}

// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:
